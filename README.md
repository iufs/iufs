# InterUniversal File System
The **InterUniversal File System** (**IUFS**) is a distributed, peer-to-peer file system that is based on a distributed hash table and is designed to provide fast and reliable content delivery for files (e.g. article images) that are too large to be stored in e.g. any blockchain. IUFS especially focus on scalability of the network as well as robustness in terms of data integrity and availability.

The IUFS project is inspired by the ,,InterPlanetary File System'' (IPFS) and is an easy to use Java framework based on [Jadex](https://www.activecomponents.org/#/project/news).

The IUFS project has these seven major aims:

* [x] Optimal load balancing and minimal disruption on communication
* [x] Recognize and block incoming malware
* [x] Delete unused files
* [x] Spam protection
* [x] Revocation certificates to delete files
* [x] Lossless compression
* [x] Prevent man-in-the-middle attacks


Therefore the project use an implementation of distributed hash tables (DHTs) in terms of minimizing communication and reorganization overhead.

# Contribute
If you have any suggestions or complaints, please [create an issue](https://gitlab.com/iufs/iufs/issues/new).

# Latest Files

* [IUFS](https://gitlab.com/iufs/iufs/-/jobs/artifacts/master/file/target/iufs-latest-full.zip?job=build%20iufs)
