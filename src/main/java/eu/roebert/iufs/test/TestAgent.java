//package eu.roebert.iufs.test;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import eu.roebert.iufs.core.IIUFSService;
//import eu.roebert.iufs.exceptions.PropertiesException;
//import eu.roebert.iufs.io.IOService;
//import eu.roebert.iufs.model.FileContainer;
//import eu.roebert.iufs.model.FileFlag;
//import eu.roebert.iufs.model.FileID;
//import eu.roebert.iufs.properties.IPropertiesService;
//import jadex.bridge.service.annotation.Service;
//import jadex.micro.annotation.Agent;
//import jadex.micro.annotation.AgentBody;
//import jadex.micro.annotation.AgentService;
//import jadex.micro.annotation.Binding;
//import jadex.micro.annotation.RequiredService;
//import jadex.micro.annotation.RequiredServices;
//
///**
// * This is a test agent that is used the way the user does it.
// */
//@Agent
//@Service
//@RequiredServices({
//        @RequiredService(name = "iufsservice", type = IIUFSService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "ioservice", type = IOService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
//public class TestAgent {
//    private static final Logger LOG = LoggerFactory.getLogger(TestAgent.class);
//
//    @AgentService(lazy = false)
//    IIUFSService iufsservice;
//
//    @AgentService(lazy = false)
//    IOService ioservice;
//
//    @AgentService(lazy = false)
//    IPropertiesService propertiesservice;
//
//    public TestAgent() {
//    }
//
//    @AgentBody
//    public void onAgentBody() {
//        saveTest();
////        bestBufferTest();
//    }
//    
//    public void saveTest() {
//        new Thread(() -> {
//            FileContainer f = new FileContainer("C:/IUFS/Testfiles/picture.jpg");
//            System.err.println(f.getID().getId());
//            System.err.println(f.getAbsolutePath());
//    
//            FileFlag[] flags = { 
//                    new FileFlag("1", "a"),
//                    new FileFlag("2", "b") };
//            
//            System.err.println(flags[0].toString());
//            System.err.println(flags[1].toString());
//            
//            iufsservice.upload(f).addIntermediateResultListener(id -> {
//                if (id.getKey() != null) {
//                    System.err.println("File uploaded and available under id: " + id.getKey());
//    
//                    System.err.println("Flags could be set: " + iufsservice.setFlags(id.getKey(), flags).get());
//                    
//                    System.err.println("Flag 1: " + iufsservice.getFlag(id.getKey(), "1", "DEF").get());
//                    System.err.println("Flag 2: " + iufsservice.getFlag(id.getKey(), "2", "DEF").get());
//                    
//                    iufsservice.download(id.getKey()).addResultListener(file -> {
//                        if (file != null) {
//                            System.err.println("File downloaded and saved under path: " + file.getAbsolutePath());
//                            
//                            System.err.println("File could be deleted: " + iufsservice.delete(id.getKey()).get());
//                        }
//                    });
//                }
//            });
//        }).start();
//    }
//
//    private void bestBufferTest() {
//        FileContainer f = new FileContainer("E:/IUFS/Testfiles/100MB.bin");
//
//        long bestTime = Long.MAX_VALUE;
//        int bestBufferSize = Integer.MIN_VALUE;
//
//        for (int i = 3; i <= 20; i++) {
//            try {
//                propertiesservice.store("bufferSize", String.valueOf((int) Math.pow(2, i)));
//            } catch (PropertiesException e1) {
//                e1.printStackTrace();
//            }
//            long start = System.currentTimeMillis();
//
//            FileID id = iufsservice.upload(f).get().iterator().next().getKey();
//
//            long end = System.currentTimeMillis();
//            long time = (end - start);
//
//            if (time < bestTime) {
//                bestTime = time;
//                bestBufferSize = (int) Math.pow(2, i);
//            }
//            ioservice.delete(id).get();
//        }
//
//        LOG.info("The best buffer size is {} with a time consumption of {}ms for a 100MiB file.", bestBufferSize,
//                bestTime);
//    }
//}
