package eu.roebert.iufs.filedb;

import java.util.List;

import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.UniversalSignable;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.IFuture;

@Service
public interface IFileDBService {

    /**
     * Puts a value in the DB
     * 
     * @param fileID the key to insert
     * @return true iff the key didn't already exist and the insertion was 
     *         successful
     */
    IFuture<Boolean> insert(FileID fileID);

    /**
     * Returns a list of all fileIDs
     * 
     * @return list of all fileIDs
     */
    IFuture<List<FileID>> getAllKeys();
    
    /**
     * Removes a value from the DB
     * 
     * @param fileID the value to delete
     * @return true iff the key existed and the deletion was successful
     */
    IFuture<Boolean> delete(FileID fileID);

    /**
     * @param key          the key for the flag
     * @param id           Unique file identification
     * @param defaultValue the default value that should be set if nothing was found
     * @return Returns the file flag
     */
    public IFuture<String> getFlag(FileID id, String key, String defaultValue);

    /**
     * @param id Unique file identification
     * @return Returns a list of file flags
     */
    public IFuture<List<FileFlag>> getFlags(FileID id);

    /**
     * Adds a flag to the file under the given unique file identification.
     * 
     * @param id   Unique file identification
     * @param flag File flag
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlag(FileID id, FileFlag flag);

    /**
     * Adds flags to the file under the given unique file identification.
     * 
     * @param id    Unique file identification
     * @param flags List of file flags
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlags(FileID id, List<FileFlag> flags);

    /**
     * Adds flags to the file under the given unique file identification.
     * 
     * @param id    Unique file identification
     * @param flags List of file flags
     * @param owner the service id of the owner
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlagsSecured(FileID id, UniversalSignable<FileFlag[]> flags, IServiceIdentifier owner);
}
