package eu.roebert.iufs.filedb;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import org.cadeia.blockchain.crypto.CryptoException;
import org.iq80.leveldb.CompressionType;
import org.iq80.leveldb.DB;
import org.iq80.leveldb.DBException;
import org.iq80.leveldb.DBIterator;
import org.iq80.leveldb.Options;
import org.iq80.leveldb.ReadOptions;
import org.iq80.leveldb.impl.Iq80DBFactory;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import eu.roebert.iufs.io.LockedFlags;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.PubKey;
import eu.roebert.iufs.model.UniversalSignable;
import eu.roebert.iufs.neighbours.INeighbourhoodService;
import eu.roebert.iufs.security.ICryptoService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.search.SServiceProvider;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@ProvidedServices({
        @ProvidedService(name = "filedbservice", type = IFileDBService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "cryptoservice", type = ICryptoService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class FileDBAgent implements IFileDBService {
    private static final Logger LOG = LoggerFactory.getLogger(FileDBAgent.class);
    protected static DB fileDB;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    @AgentService(lazy = false)
    ICryptoService cryptoservice;

    public FileDBAgent() {
    }

    @AgentCreated
    public void created() {
        Optional<String> fileDBPath = Optional.fromNullable((String) arguments.getArguments().get("fileDBPath"));
        onServiceStart(new File(fileDBPath.or("db/files.db"))).get();

        LOG.info("FileDBAgent created.");
    }

    @AgentKilled
    public void onAgentKilled() {
        try {
            fileDB.close();
            LOG.info("File DB connection closed.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private IFuture<Void> onServiceStart(File fileDBPath) {
        Future<Void> rtn = new Future<>();

        try {
            Options options = new Options();
            options.createIfMissing(true);
            options.logger(message -> LOG.info(message));
            options.compressionType(CompressionType.SNAPPY);

            LOG.info("{}: opening dbs at: {}", this, fileDBPath);
            fileDB = Iq80DBFactory.factory.open(fileDBPath, options);

            LOG.info("{}: Opened dbs {}", this, fileDBPath);
        } catch (Exception e) {
            LOG.error("{}: Couldn't open level-db", this, e);
            rtn.setException(e);
            e.printStackTrace();
            System.exit(1);
        }

        rtn.setResult((Void) null);
        return rtn;
    }

    @Override
    public IFuture<Boolean> insert(FileID fileID) {
        if (getValue(fileID) != null) {
            return Future.FALSE;
        }
        return new Future<Boolean>(insert(fileID, ""));
    }

    /**
     * Puts a key-value pair in the DB
     * 
     * @param fileID the key to insert
     * @param flag   the value to insert
     */
    private boolean insert(FileID fileID, String flag) {
        try {
            fileDB.put(fileID.getId().getBytes(), flag.getBytes());
            LOG.info("Inserted ({}, {}) into the file DB", fileID, flag);
            return true;
        } catch (DBException e) {
            return false;
        }
    }

    /**
     * Returns a the corresponding flags to a given file ID
     * 
     * @param fileIDs the key
     * @return the value. null if the value couldn't be retrieved or is not in the
     *         database
     */
    private String getValue(FileID fileID) {
        try {
            byte[] bytes = fileDB.get(fileID.getId().getBytes());
            if (bytes == null) {
                return null;
            }
            return new String(bytes);
        } catch (DBException e) {
            return null;
        }
    }

    @Override
    public IFuture<List<FileID>> getAllKeys() {
        ReadOptions readOptions = new ReadOptions().snapshot(fileDB.getSnapshot());
        DBIterator iterator = fileDB.iterator(readOptions);
        List<FileID> list = new ArrayList<FileID>();
        iterator.seekToFirst();
        iterator.forEachRemaining(entry -> {
            byte[] v = entry.getKey();
            list.add(new FileID(new String(v)));
        });
        try {
            iterator.close();
        } catch (IOException e) {
            Log.info("Iterator couldn't be closed.");
            e.printStackTrace();
        }

        return new Future<>(list);
    }

    @Override
    public IFuture<Boolean> delete(FileID fileID) {
        try {
            if (getValue(fileID) == null) {
                return Future.FALSE;
            }
            fileDB.delete(fileID.getId().getBytes());
            LOG.info("Deleted {} from the file DB", fileID);

            return Future.TRUE;

        } catch (DBException e) {
            return Future.FALSE;
        }
    }

    @Override
    public IFuture<String> getFlag(FileID id, String key, String defaultValue) {
        String value = getValue(id);
        if (value == null) {
            return new Future<>(defaultValue);
        }
        Properties props = new Properties();
        StringReader sr = new StringReader(value);
        try {
            props.load(sr);
        } catch (IOException e) {
            return new Future<>(defaultValue);
        }
        String result = props.getProperty(key);
        if (result == null) {
            return new Future<>(defaultValue);
        }

        return new Future<>(result);
    }

    @Override
    public IFuture<List<FileFlag>> getFlags(FileID id) {
        ArrayList<FileFlag> flags = new ArrayList<FileFlag>();
        String value = getValue(id);
        if (value == null) {
            return new Future<>(flags);
        }
        Properties props = new Properties();
        StringReader sr = new StringReader(value);
        try {
            props.load(sr);
        } catch (IOException e) {
            return new Future<>(flags);
        }
        for (String key : props.stringPropertyNames()) {
            flags.add(new FileFlag(key, props.getProperty(key)));
        }

        return new Future<>(flags);
    }

    @Override
    public IFuture<Boolean> setFlag(FileID id, FileFlag flag) {
        return setFlags(id, Arrays.asList(flag));
    }

    @Override
    public IFuture<Boolean> setFlags(FileID id, List<FileFlag> flags) {
        String value = getValue(id);
        if (value == null) {
            return Future.FALSE;
        }
        Properties props = new Properties();
        StringReader sr = new StringReader(value);
        try {
            props.load(sr);
            for (FileFlag flag : flags) {
                props.setProperty(flag.getFlag(), flag.getValue());
            }
            StringWriter sw = new StringWriter();
            props.store(sw, "Flags for file " + id.getId());
            insert(id, sw.toString());
        } catch (IOException e) {
            return Future.FALSE;
        }

        return Future.TRUE;
    }

    @Override
    public IFuture<Boolean> setFlagsSecured(FileID id, UniversalSignable<FileFlag[]> signedFlags,
            IServiceIdentifier owner) {
        Future<Boolean> rtn = new Future<>();

        if (getValue(id) == null) {
            return Future.FALSE;
        }
        new Thread(() -> {
            try {
                INeighbourhoodService service = null;
                while (true) {
                    try {
                        service = (INeighbourhoodService) SServiceProvider.getService(agent, owner, true).get();
                        break;
                    } catch (RuntimeException e) {
                        continue;
                    }
                }
                
                PubKey ownerPubKey = new PubKey(getFlag(id, "owner", "").get());

                if (signedFlags.getSignatory().equals(ownerPubKey)
                        && cryptoservice.verifySignature(ownerPubKey, signedFlags).get()
                        && ownerPubKey.equals(service.getPubKey().get())) {

                    List<FileFlag> flags = Arrays.asList(signedFlags.getObject());
                    for (FileFlag flag : flags) {
                        if (LockedFlags.contains(flag)) {
                            flags.remove(flag);
                        }
                    }
                    rtn.setResult(setFlags(id, flags).get());
                }
            } catch (CryptoException e) {
                e.printStackTrace();
            }
        }).start();

        return rtn;
    }
}