package eu.roebert.iufs.exceptions;

public class AVException extends Exception {
    private static final long serialVersionUID = -6568050567208886235L;

    public AVException() {
        super();
    }

    public AVException(String message) {
        super(message);
    }
}
