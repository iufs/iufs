package eu.roebert.iufs.exceptions;

public class PropertiesException extends Exception {
    private static final long serialVersionUID = 319795340211970939L;

    public PropertiesException() {
        super();
    }

    public PropertiesException(String message) {
        super(message);
    }
}
