package eu.roebert.iufs.avscan;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.exceptions.AVException;
import eu.roebert.iufs.properties.IPropertiesService;
import fi.solita.clamav.ClamAVClient;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.annotation.Service;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

/**
 * This agent implements the ClamAV Scanner. It works with the ClamAV daemon.
 * For this agent to work properly, the ClamAV daemon must be running in the
 * background on the specified {@link ClamAVAgent#host} and
 * {@link ClamAVAgent#port}.
 * 
 * Under Windows you can download ClamAV here: https://www.clamav.net/downloads.
 * Under Windows you also have to manually start the ClamAV daemon (clamd.exe)
 * or you create manually once a service that is automatically started on
 * bootup. For this purpose you can use the NSSM - the Non-Sucking Service
 * Manager (https://nssm.cc/).
 * 
 * Under Unix, the ClamAV daemon should started automatically on bootup.
 * 
 * <b>It is up to the user of this agent to ensure that the ClamAV virus
 * database is regularly updated.</b>
 */
@Agent
@Service
@ProvidedServices({ @ProvidedService(name = "clamav", type = AVScanService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class ClamAVAgent implements AVScanService {
    private static final Logger LOG = LoggerFactory.getLogger(ClamAVAgent.class);
    protected ClamAVClient clamClient;
    protected String host;
    protected int port;
    protected boolean enabled;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    public ClamAVAgent() {
    }

    @AgentCreated
    public void created() {
        LOG.debug("Agent created");
        host = propertiesservice.get("clamAVHost").get();
        port = propertiesservice.getInt("clamAVPort").get();
        enabled = propertiesservice.getBool("clamAVEnabled").get();
        clamClient = new ClamAVClient(host, port);
    }

    @Override
    public boolean scan(String nodeID, String path) throws AVException {
        try {
            FileInputStream fis = new FileInputStream(new File(path));
            boolean result = scan(fis);
            fis.close();

            return result;
        } catch (IOException e) {
            e.printStackTrace();
            throw new AVException(e.getMessage());
        }
    }

    /**
     * Scans an InputStream for virus by passing the bytes to an AVScanService
     * agent.
     * 
     * @param is InputStream, that should be scanned
     * @return true if no virus was found
     * @throws AVException if data could not scanned
     */
    public boolean scan(InputStream is) throws AVException {
        try {
            byte[] reply = clamClient.scan(is);

            if (ClamAVClient.isCleanReply(reply)) {
                is.close();
                return true;
            } else {
                LOG.warn("Found virus with ClamAV.");
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new AVException("Could not scan the input.");
        }

        return false;
    }

    /**
     * Check if the scanner is available.
     * 
     * @return true if the scanner is available
     */
    public boolean available() {
        try {
            return enabled && clamClient.ping();
        } catch (Exception e) {
            return false;
        }
    }
}
