package eu.roebert.iufs.avscan;

import java.util.ArrayList;
import java.util.Collection;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.exceptions.AVException;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.IIntermediateResultListener;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@ProvidedServices({
        @ProvidedService(name = "avmiddleware", type = AVMiddlewareService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "avservices", type = AVScanService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "avdatabase", type = IAVDatabaseService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class AVMiddlewareAgent implements AVMiddlewareService {
    private static final Logger log = LoggerFactory.getLogger(AVMiddlewareAgent.class);
    protected ArrayList<AVScanService> avs;

    @AgentService(lazy = false)
    protected IAVDatabaseService avdatabase;

    @AgentFeature
    IRequiredServicesFeature requiredServices;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    public AVMiddlewareAgent() {
    }

    @AgentCreated
    public void created() {
        log.debug("Agent created");
        avs = new ArrayList<>();

        requiredServices.getRequiredServices("avservices").addResultListener(new IIntermediateResultListener<Object>() {

            @Override
            public void resultAvailable(Collection<Object> result) {
            }

            @Override
            public void exceptionOccurred(Exception exception) {
            }

            @Override
            public void intermediateResultAvailable(Object result) {
                AVScanService av = (AVScanService) result;

                if (av.available()) {
                    avs.add(av);
                    log.info("Found an active AV service.");
                }
            }

            @Override
            public void finished() {
            }

        });
    }

    @Override
    public boolean scan(String nodeID, String path) throws AVException {
        boolean result = true;

        for (AVScanService av : avs) {
            if (av.available() && !av.scan(nodeID, path)) {
                result = false;
            }
        }

        if (result) {
            avdatabase.increment(nodeID).get();
        }

        return result;
    }

}
