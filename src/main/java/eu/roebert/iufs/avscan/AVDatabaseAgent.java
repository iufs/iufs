package eu.roebert.iufs.avscan;

import java.io.File;
import java.nio.charset.StandardCharsets;

import org.iq80.leveldb.CompressionType;
import org.iq80.leveldb.DB;
import org.iq80.leveldb.Options;
import org.iq80.leveldb.impl.Iq80DBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import eu.roebert.iufs.common.helper.ByteUtils;
import eu.roebert.iufs.exceptions.AVException;
import eu.roebert.iufs.properties.IPropertiesService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@ProvidedServices({ @ProvidedService(type = AVScanService.class, scope = Binding.SCOPE_PLATFORM),
        @ProvidedService(type = IAVDatabaseService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class AVDatabaseAgent implements AVScanService, IAVDatabaseService {
    private static final Logger LOG = LoggerFactory.getLogger(AVDatabaseAgent.class);
    private DB avDB;
    private int limit;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    public AVDatabaseAgent() {
    }

    @AgentCreated
    public void created() {
        Optional<String> avDBPath = Optional.fromNullable((String) arguments.getArguments().get("avDBPath"));
        onServiceStart(new File(avDBPath.or("db/av.db"))).get();

        limit = propertiesservice.getInt("avLimit").get();

        LOG.info("AVDatabaseAgent created.");
    }

    @Override
    public boolean scan(String nodeID, String path) throws AVException {
        byte[] node = avDB.get(nodeID.getBytes(StandardCharsets.UTF_8));

        if (node != null) {
            int count = ByteUtils.byte2int(node);

            return count >= limit;
        } else {
            return false;
        }
    }

    @Override
    public boolean available() {
        // TODO Auto-generated method stub
        return false;
    }

    private IFuture<Void> onServiceStart(File avDBPath) {
        Future<Void> rtn = new Future<>();

        try {
            Options options = new Options();
            options.createIfMissing(true);
            options.logger(message -> LOG.info(message));
            options.compressionType(CompressionType.SNAPPY);

            LOG.info("{}: opening dbs at: {}", this, avDBPath);
            avDB = Iq80DBFactory.factory.open(avDBPath, options);

            LOG.info("{}: Opened dbs {}", this, avDBPath);
        } catch (Exception e) {
            LOG.error("{}: Couldn't open level-db", this, e);
            rtn.setException(e);
            e.printStackTrace();
            System.exit(1);
        }

        rtn.setResult((Void) null);
        return rtn;
    }

    @Override
    protected void finalize() throws Throwable {
        LOG.info("Closing Database");
        if (avDB != null) {
            avDB.close();
        }
    }

    @Override
    public IFuture<Void> increment(String nodeID) {
        byte[] id = nodeID.getBytes(StandardCharsets.UTF_8);
        byte[] node = avDB.get(id);
        int count = 0;

        if (node != null) {
            count = ByteUtils.byte2int(node);
        }

        avDB.put(id, ByteUtils.int2byte(count));

        return Future.DONE;
    }
}
