package eu.roebert.iufs.avscan;

import eu.roebert.iufs.exceptions.AVException;
import jadex.bridge.service.annotation.Service;

@Service
public interface AVMiddlewareService {

    /**
     * Scans a stream of bytes for virus by passing the bytes to all activated
     * AVScanService agents.
     * 
     * @param nodeID the id of the node, that has send the file
     * @param path   path to file, that should be scanned
     * @return true if no virus was found
     * @throws AVException if data could not scanned
     */
    public boolean scan(String nodeID, String path) throws AVException;
}
