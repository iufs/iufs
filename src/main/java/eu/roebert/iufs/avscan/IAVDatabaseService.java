package eu.roebert.iufs.avscan;

import jadex.commons.future.IFuture;

public interface IAVDatabaseService {
    /**
     * Increments the counter of {@code nodeID} by one.
     * 
     * @param nodeID id of the node
     * @return A future that is already done
     */
    public IFuture<Void> increment(String nodeID);
}
