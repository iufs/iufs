package eu.roebert.iufs.avscan;

import eu.roebert.iufs.exceptions.AVException;
import jadex.bridge.service.annotation.Service;

@Service
public interface AVScanService {

    /**
     * Scans a file for virus by passing the bytes to an AVScanService agent.
     * 
     * @param nodeID the id of the node, that has send the file
     * @param path   path to file, that should be scanned
     * @return true if no virus was found
     * @throws AVException if data could not scanned
     */
    public boolean scan(String nodeID, String path) throws AVException;

    /**
     * @return Returns true, if the Service is available on this machine and
     *         enabled.
     */
    public boolean available();
}
