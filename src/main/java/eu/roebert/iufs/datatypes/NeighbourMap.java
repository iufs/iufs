package eu.roebert.iufs.datatypes;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class NeighbourMap<K, V> {
    private int size;
    private int DEFAULT_CAPACITY = 16;
    @SuppressWarnings("unchecked")
    private NeighbourEntry<K, V>[] values = new NeighbourEntry[DEFAULT_CAPACITY];

    public NeighbourMap() {
    }

    public V get(K key) {
        for (int i = 0; i < size; i++) {
            if (values[i] != null) {
                if (values[i].getKey().equals(key)) {
                    return values[i].getValue();
                }
            }
        }
        return null;
    }

    public boolean containsKey(K key) {
        for (NeighbourEntry<K, V> entry : values) {
            if (entry.getKey().equals(key)) {
                return true;
            }
        }

        return false;
    }

    public void put(K key, V value) {
        boolean insert = true;
        for (int i = 0; i < size; i++) {
            if (values[i].getKey().equals(key)) {
                values[i].setValue(value);
                insert = false;
            }
        }
        if (insert) {
            ensureCapa();
            values[size++] = new NeighbourEntry<K, V>(key, value);
        }
    }

    private void ensureCapa() {
        if (size == values.length) {
            int newSize = values.length * 2;
            values = Arrays.copyOf(values, newSize);
        }
    }

    public int size() {
        return size;
    }

    public void remove(K key) {
        for (int i = 0; i < size; i++) {
            if (values[i].getKey().equals(key)) {
                values[i] = null;
                size--;
                condenseArray(i);
            }
        }
    }

    private void condenseArray(int start) {
        for (int i = start; i < size; i++) {
            values[i] = values[i + 1];
        }
    }

    public Set<NeighbourEntry<K, V>> entrySet() {
        Set<NeighbourEntry<K, V>> set = new HashSet<NeighbourEntry<K, V>>();
        for (int i = 0; i < size; i++) {
            set.add(values[i]);
        }
        return set;
    }
}
