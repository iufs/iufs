package eu.roebert.iufs.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.channels.Channels;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.cadeia.blockchain.crypto.CryptoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Arrays;

import eu.roebert.iufs.avscan.AVMiddlewareService;
import eu.roebert.iufs.compression.CompressionFactory;
import eu.roebert.iufs.exceptions.AVException;
import eu.roebert.iufs.filedb.IFileDBService;
import eu.roebert.iufs.future.IntermediateStreamResultListener;
import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.PubKey;
import eu.roebert.iufs.model.UniversalSignable;
import eu.roebert.iufs.neighbours.INeighbourhoodService;
import eu.roebert.iufs.properties.IPropertiesService;
import eu.roebert.iufs.security.ICryptoService;
import eu.roebert.crypto.crypto.Hashing;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.Timeout;
import jadex.bridge.service.search.SServiceProvider;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.commons.future.ISubscriptionIntermediateFuture;
import jadex.commons.future.SubscriptionIntermediateFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.bridge.service.IServiceIdentifier;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@ProvidedServices({ @ProvidedService(name = "ioservice", type = IOService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "avmiddleware", type = AVMiddlewareService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "cryptoservice", type = ICryptoService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "filedbservice", type = IFileDBService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class IOFileSystemAgent implements IOService {
    private static final Logger LOG = LoggerFactory.getLogger(IOFileSystemAgent.class);
    private String tempDir;
    protected String rootDir;
    protected List<FileContainer> approvedPaths;
    protected HashMap<String, ISubscriptionIntermediateFuture<byte[]>> listeners;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @AgentService(lazy = false)
    AVMiddlewareService avmiddleware;

    @AgentService(lazy = false)
    ICryptoService cryptoservice;

    @AgentService(lazy = false)
    IFileDBService filedbservice;

    @Agent
    IInternalAccess agent;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    public IOFileSystemAgent() {
        approvedPaths = new ArrayList<>();
        listeners = new HashMap<>();
    }

    @AgentCreated
    public void created() {
        rootDir = propertiesservice.get("rootDir").get();
        tempDir = rootDir + "/temp";

        LOG.debug("Agent created");
    }

    private File createTempFile(String ext) throws IOException {
        File tempFileDir = new File(tempDir);
        tempFileDir.mkdirs();
        File tempFile = File.createTempFile("IUFSFile", "." + ext, tempFileDir);
        tempFile.deleteOnExit();

        return tempFile;
    }

    private File getIUFSFileFromID(FileID id) {
        return new File(rootDir + "/" + id.getId() + "/" + id.getId() + ".iufs");
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<FileID> save(IServiceIdentifier serviceID, UniversalSignable<FileContainer> file, String ext) {
        Future<FileID> rtn = new Future<>();

        new Thread(() -> {
            try {
                PubKey ownerPubKey = file.getSignatory();

                if (cryptoservice.verifySignature(ownerPubKey, file).get()) {
                    long start = System.currentTimeMillis();
                    File tempFile = createTempFile(ext);
                    OutputStream out = new FileOutputStream(tempFile);
                    OutputStream bos;
                    DigestOutputStream dos;

                    int bufferSize = propertiesservice.getInt("bufferSize").get();
                    INeighbourhoodService service = null;
                    while (true) {
                        try {
                            service = (INeighbourhoodService) SServiceProvider.getService(agent, serviceID, true).get();
                            break;
                        } catch (RuntimeException e) {
                            continue;
                        }
                    }

                    // Otherwise Jadex can't transfer the byte-Array
                    if (bufferSize > 1048576) {
                        bufferSize = 1048576;
                    }

                    ISubscriptionIntermediateFuture<byte[]> fileChunks = service.getFile(file.getObject(), bufferSize);
                    String listenerID = UUID.randomUUID().toString();
                    listeners.put(listenerID, fileChunks);

                    try {
                        dos = new DigestOutputStream(out, MessageDigest.getInstance("SHA-256"));
                        bos = new BufferedOutputStream(dos, 1024 * 1024);

                        fileChunks.addResultListener(new IntermediateStreamResultListener<byte[]>(bos, dos, rtn) {
                            @Override
                            public void exceptionOccurred(Exception exception) {
                                LOG.error(exception.getMessage());
                                exception.printStackTrace();
                                listeners.remove(listenerID);
                                try {
                                    bos.close();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void resultAvailable(Collection<byte[]> result) {
                            }

                            @Override
                            public void intermediateResultAvailable(byte[] result) {
                                try {
                                    bos.write(result, 0, result.length);
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    fileChunks.terminate(e);
                                    rtn.setException(new IOException(e.getMessage()));
                                }
                            }

                            @Override
                            public void finished() {
                                long end = System.currentTimeMillis();
                                LOG.debug("File was sent in " + (end - start) + "ms");
                                try {
                                    bos.flush();
                                    INeighbourhoodService service = null;
                                    while (true) {
                                        try {
                                            service = (INeighbourhoodService) SServiceProvider
                                                    .getService(agent, serviceID, true).get();
                                            break;
                                        } catch (RuntimeException e) {
                                            continue;
                                        }
                                    }

                                    if (scan(service.getUUID().get(), tempFile.getAbsolutePath()).get()) {
                                        FileID id = new FileID(Hashing.sha256(dos));
                                        bos.close();
                                        if (id.equals(file.getObject().getID())) {
                                            filedbservice.insert(id).get();
                                            move(id, ext, tempFile, ownerPubKey);
                                            o3.setResult(id);
                                        } else {
                                            tempFile.delete();
                                            rtn.setException(
                                                    new CryptoException("The file is corrupted.", new Exception()));
                                        }
                                    } else {
                                        bos.close();
                                        tempFile.delete();
                                    }
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    fileChunks.terminate(e);
                                    rtn.setException(e);
                                }
                                listeners.remove(listenerID);
                            }
                        });
                    } catch (NoSuchAlgorithmException e) {
                        IOException error = new IOException("Can't found SHA-256 algorithm.");
                        LOG.error("Can't found SHA-256 algorithm.");
                        fileChunks.terminate(error);
                        throw error;
                    }
                } else {
                    rtn.setException(new CryptoException("The signature is invalid.", new Exception()));
                }
            } catch (IOException | CryptoException e) {
                e.printStackTrace();
            }
        }).start();

        return rtn;
    }

    /**
     * Scans a file with the AVMiddlewareService, in the local thread.
     * 
     * @param nodeID the id of the node, that has send the file
     * @param path   to file
     * @return IFuture.FALSE if the file is infected, else IFuture.TRUE
     */
    protected IFuture<Boolean> scan(String nodeID, String path) {
        try {
            return new Future<>(avmiddleware.scan(nodeID, path));
        } catch (AVException e) {
            e.printStackTrace();
            return new Future<>(true);
        }
    }

    /**
     * Moves the file to the dest dir and compress the file before.
     * 
     * @param dos      DigestOutputStream for on the fly hashing
     * @param ext      the file extension
     * @param tempFile the temp file object that should be moved
     * @param owner    the public key from the file owner
     * @throws IOException if an I/O error occurs
     */
    private void move(FileID id, String ext, File tempFile, PubKey owner) throws IOException {
        new File(rootDir + "/" + id.getId() + "/").mkdirs();
        File dest = getIUFSFileFromID(id);

        try {
            CompressionFactory.get("lz4").compress(tempFile, dest);
            tempFile.delete();
            FileUtils.deleteQuietly(tempFile);
            List<FileFlag> fileFlags = Arrays.asList(new FileFlag(LockedFlags.extension.name(), ext),
                    new FileFlag(LockedFlags.lastAccess.name(), String.valueOf(System.currentTimeMillis())),
                    new FileFlag(LockedFlags.compression.name(), "lz4"),
                    new FileFlag("owner", owner.getCompressedKey()));

            filedbservice.setFlags(id, fileFlags).get();
        } catch (FileExistsException e) {
            LOG.debug("File " + id.getId() + ".iufs already exists.");
        } catch (IOException e) {
            e.printStackTrace();
            LOG.error("File " + id.getId() + ".iufs can't moved to the dest dir.");
            throw new IOException("File " + id.getId() + ".iufs can't moved to the dest dir.");
        }
    }

    @Override
    public IFuture<Boolean> fileExists(FileID id) {
        return new Future<>(getIUFSFileFromID(id).exists());
    }

    @Override
    public IFuture<Boolean> delete(FileID id) {
        try {
            FileUtils.deleteDirectory(new File(rootDir + "/" + id.getId()));
            filedbservice.delete(id);
            LOG.info("Deleted file with ID: ", id.getId());
        } catch (IOException e) {
            return Future.FALSE;
        }

        return Future.TRUE;
    }

    @Override
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getStream(FileID id, int bffSize) throws IOException {
        if (!fileExists(id).get()) {
            throw new IOException("File does not exists on the file system.");
        }
        FileContainer file = new FileContainer(getIUFSFileFromID(id));
        filedbservice.setFlag(id, new FileFlag("lastAccess", String.valueOf(System.currentTimeMillis()))).get();

        approvePath(file).get();

        return getFile(file, bffSize);
    }

    @Override
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getFile(FileContainer file, int bffSize) throws IOException {
        SubscriptionIntermediateFuture<byte[]> fileStream = new SubscriptionIntermediateFuture<>();

        new Thread(() -> {
            if (approvedPaths.contains(file)) {
                approvedPaths.remove(file);
                try (RandomAccessFile raf = new RandomAccessFile(file.toFile(), "r")) {
                    InputStream is = Channels.newInputStream(raf.getChannel());
                    byte buffer[] = new byte[bffSize];
                    BufferedInputStream bis = new BufferedInputStream(is, 1024 * 1024 * 2);
                    long send = 0;
                    int c = 0;
                    while ((c = bis.read(buffer)) != -1) {
                        fileStream.addIntermediateResult(Arrays.copyOf(buffer, c));
                        send += c;
                    }

                    fileStream.setFinished();
                    bis.close();

                    LOG.debug("In total, {} bytes were sent.", send);
                } catch (IOException e) {
                    fileStream.setException(e);
                    fileStream.setFinished();
                }
            } else {
                fileStream.setException(new IllegalArgumentException("The given path is not accessible from outside."));
                fileStream.setFinished();
            }
        }).start();

        return fileStream;
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<String> getTempFile(FileID fileID, String ext, String compression) throws IOException {
        File sourceFile = getIUFSFileFromID(fileID);
        File tempFile = createTempFile(ext);

        if (fileExists(fileID).get()) {
            CompressionFactory.get(compression).decompress(sourceFile, tempFile);
        } else {
            throw new IOException("The file with the given ID doesn't exist.");
        }

        return new Future<>(tempFile.getAbsolutePath());
    }

    @Override
    public IFuture<Void> approvePath(FileContainer file) {
        approvedPaths.add(file);

        return Future.DONE;
    }

    @Override
    public IFuture<Boolean> deleteSecured(UniversalSignable<FileID> id, IServiceIdentifier owner) {
        Future<Boolean> rtn = new Future<>();

        new Thread(() -> {
            try {
                INeighbourhoodService service = null;
                while (true) {
                    try {
                        service = (INeighbourhoodService) SServiceProvider.getService(agent, owner, true).get();
                        break;
                    } catch (RuntimeException e) {
                        continue;
                    }
                }

                PubKey ownerPubKey = new PubKey(filedbservice.getFlag(id.getObject(), "owner", "").get());

                if (id.getSignatory().equals(ownerPubKey) && cryptoservice.verifySignature(ownerPubKey, id).get()
                        && ownerPubKey.equals(service.getPubKey().get())) {
                    rtn.setResult(delete(id.getObject()).get());
                }
            } catch (CryptoException e) {
                e.printStackTrace();
            }
        }).start();

        return rtn;
    }

    @Override
    public void finalize() {
        try {
            FileUtils.deleteDirectory(new File(tempDir));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
