package eu.roebert.iufs.io;

import java.io.IOException;

import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.UniversalSignable;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;
import jadex.commons.future.ISubscriptionIntermediateFuture;

/**
 * This Interface is responsible for all I/O-Operations.
 */
@Service
public interface IOService {

    /**
     * Approves a path to be accessible, from outside, once.
     * 
     * @param file FileContainer of the file
     * @return a future that is already done
     */
    public IFuture<Void> approvePath(FileContainer file);

    /**
     * Saves a file with given extension and compress the file by LZ4 Frame format.
     * 
     * @param serviceID the id of the service
     * @param file      signed FileContainer of file
     * @param ext       file extension
     * @return unique file identification
     * @throws IOException if an I/O error occurs
     */
    @Timeout(Timeout.NONE)
    public IFuture<FileID> save(IServiceIdentifier serviceID, UniversalSignable<FileContainer> file, String ext);

    /**
     * This method is used to transform a file under a given unique file
     * identification into an ISubscriptionIntermediateFuture stream. This stream
     * can passed used by the
     * {@link #save(IServiceIdentifier, FileContainer, String) save} method to
     * upload a file onto a node.
     * 
     * @param id         unique file identification
     * @param bufferSize the size of the buffer that the receiver expect
     * @throws IOException If an I/O error occurs
     * @return fileStream as SubscriptionIntermediateFuture byte array stream
     *         compressed by LZ4 Frame format
     */
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getStream(FileID id, int bufferSize) throws IOException;

    /**
     * This method is used to transform a file under a given path into an
     * ISubscriptionIntermediateFuture stream. TThis stream can passed used by the
     * {@link #save(IServiceIdentifier, FileContainer, String) save} method to
     * upload a file onto a node.
     * 
     * @param file       FileContainer of file that shall be transformed
     * @param bufferSize the size of the buffer that the receiver expect
     * @throws IOException If an I/O error occurs
     * @return file under path as SubscriptionIntermediateFuture byte array stream
     *         compressed by LZ4 Frame format
     */
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getFile(FileContainer file, int bufferSize) throws IOException;

    /**
     * This method is used to copy a file under a given FileID into a temporary
     * file.
     * 
     * @param file        FileContainer of the new file that shall be created
     * @param ext         File extension of the file that shall be created
     * @param compression Compression type of the file that shall be created
     * @throws IOException If an I/O error occurs
     * @return new temp file
     */
    @Timeout(Timeout.NONE)
    public IFuture<String> getTempFile(FileID file, String ext, String compression) throws IOException;

    /**
     * @param id Unique file identification
     * @return Checks if a file exists on the file system
     */
    public IFuture<Boolean> fileExists(FileID id);

    /**
     * Deletes a file under the given unique file identification from the file
     * system.
     * 
     * @param id Unique file identification
     * @return true iff the key existed and the deletion was successful
     */
    public IFuture<Boolean> delete(FileID id);

    /**
     * Deletes a file under the given unique file identification from the file
     * system.
     * 
     * @param id    Unique file identification
     * @param owner the service id of the owner
     * @return true iff the key existed and the deletion was successful
     */
    public IFuture<Boolean> deleteSecured(UniversalSignable<FileID> id, IServiceIdentifier owner);
}
