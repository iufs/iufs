package eu.roebert.iufs.io;

import eu.roebert.iufs.model.FileFlag;

/**
 * This enum represents the {@link eu.roebert.iufs.model.FileFlag FileFlag}'s,
 * that can't be changed from outside.
 */
public enum LockedFlags {
    lastAccess, compression, extension;
    public static LockedFlags[] flags = LockedFlags.values();

    public static boolean contains(FileFlag fileFlag) {
        for (LockedFlags flag : flags) {
            if (flag.name().equalsIgnoreCase(fileFlag.getFlag())) {
                return true;
            }
        }

        return false;
    }
}
