package eu.roebert.iufs.common.helper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.KeyPair;
import java.util.Objects;

import org.cadeia.blockchain.serialization.json.GsonFactory;

import com.google.gson.Gson;

public final class JsonWriterUtil {
    private static final Gson gson = GsonFactory.createGson();

    public static void writeKeyPair(KeyPair keyPair, String filePath) throws IOException {
        Objects.requireNonNull(keyPair);
        Objects.requireNonNull(filePath);
        File folder = new File(filePath).getParentFile();

        if (!folder.exists()) {
            folder.mkdirs();
        }

        try (FileWriter writer = new FileWriter(filePath)) {
            gson.toJson(keyPair, writer);
        } catch (IOException e) {
            throw e;
        }
    }
}
