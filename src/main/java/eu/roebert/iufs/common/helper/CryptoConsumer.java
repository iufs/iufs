package eu.roebert.iufs.common.helper;

import org.cadeia.blockchain.crypto.CryptoException;

/**
 * Represents an operation that accepts a single input argument and returns no
 * result. Unlike most other functional interfaces, {@code CryptoConsumer} is
 * expected to operate via side-effects.
 *
 * <p>
 * This is a <a href="package-summary.html">functional interface</a> whose
 * functional method is {@link #accept(Object)}.
 *
 * @param <T> the type of the input to the operation
 *
 * @since 1.2.1
 */
@FunctionalInterface
public interface CryptoConsumer<T> {
    /**
     * Performs this operation on the given argument.
     *
     * @param t the input argument
     */
    void accept(T t) throws CryptoException;
}
