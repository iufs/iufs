package eu.roebert.iufs.common.helper;

import java.math.BigInteger;

public class ByteUtils {
    public static byte[] int2byte(int x) {
        return BigInteger.valueOf(x).toByteArray();
    }

    public static int byte2int(byte[] x) {
        return new BigInteger(x).intValue();
    }
}
