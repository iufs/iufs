package eu.roebert.iufs.model;

import jadex.commons.transformation.annotations.Include;

public class Pair<K, V> {
    @Include
    private final K key;
    @Include
    private final V value;

    public Pair(K k, V v) {
        key = k;
        value = v;
    }

    public K getKey() {
        return key;
    }

    public V getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Pair [key=" + key + ", value=" + value + "]";
    }
}
