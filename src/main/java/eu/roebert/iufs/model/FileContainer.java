package eu.roebert.iufs.model;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import eu.roebert.crypto.crypto.Hashing;
import jadex.commons.transformation.annotations.IncludeFields;

/**
 * This class saves the absolute path to a file to send files with Jadex.
 */
@IncludeFields(includePrivate = true)
public class FileContainer implements Closeable {
    private String absolutePath;
    private FileID id;

    /**
     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
     */
    public FileContainer() {
        absolutePath = "";
    }

    public FileContainer(File file) {
        absolutePath = file.toPath().normalize().toString();
        hash();
    }

    public FileContainer(File file, FileID fileID) {
        absolutePath = file.toPath().normalize().toString();
        id = fileID;
    }

    public FileContainer(File parent, String child) {
        absolutePath = new File(parent, child).toPath().normalize().toString();
        hash();
    }

    public FileContainer(String pathname) {
        absolutePath = Paths.get(pathname).normalize().toString();
        hash();
    }

    public FileContainer(String parent, String child) {
        absolutePath = new File(parent, child).toPath().normalize().toString();
        hash();
    }

    public FileContainer(URI uri) {
        absolutePath = new File(uri).toPath().normalize().toString();
        hash();
    }

    public File toFile() {
        return Paths.get(getAbsolutePath()).toFile();
    }

    public String getAbsolutePath() {
        return FilenameUtils.normalize(absolutePath);
    }

    public String getExtension() {
        return FilenameUtils.getExtension(getAbsolutePath());
    }

    public FileID getID() {
        return id;
    }

    /**
     * Hashes the wrapped file. File must be accessible on this file system.
     */
    public void hash() {
        if (toFile().exists() && id == null) {
            id = new FileID(Hashing.sha256(toFile()));
        }
    }

    @Override
    public void close() throws IOException {
        FileUtils.forceDelete(toFile());
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FileContainer) {
            FileContainer c1 = (FileContainer) o;

            return c1.getAbsolutePath().equals(getAbsolutePath()) && c1.getID().equals(getID());
        }

        return false;
    }
}
