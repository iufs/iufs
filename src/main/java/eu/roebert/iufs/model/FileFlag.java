package eu.roebert.iufs.model;

import jadex.commons.transformation.annotations.IncludeFields;

/**
 * This class is responsible for modeling a file flag.
 */
@IncludeFields(includePrivate = true)
public class FileFlag {
    private String flag;
    private String value;

    public FileFlag() {
    }

    public FileFlag(String flag, String value) {
        this.flag = flag;
        this.value = value;
    }

    /**
     * @return the flag
     */
    public String getFlag() {
        return flag;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FileFlag) {
            FileFlag f = (FileFlag) o;

            return flag.equals(f.flag) && value.equals(f.value);
        }

        return false;
    }

    @Override
    public String toString() {
        return "Flag: " + flag + ", Value: " + value;
    }
}
