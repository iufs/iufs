package eu.roebert.iufs.model;

import jadex.commons.transformation.annotations.IncludeFields;

/**
 * Class to pass an integer by reference.
 */
@IncludeFields(includePrivate = true)
public class IntRefHolder {
    public Integer value;

    public IntRefHolder() {
    }

    public IntRefHolder(Integer value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
