package eu.roebert.iufs.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.Map.Entry;

import eu.roebert.crypto.crypto.Hashing;
import jadex.bridge.service.IServiceIdentifier;

/**
 * Used to generate a list of responsible nodes via the rendezvous hashing
 * technique where node UUIDs and service identifiers can be retrieved from
 */
public class RendezvousList {
    private final TreeMap<String, Entry<String, IServiceIdentifier>> elements = new TreeMap<>();

    /**
     * Creates a list of entries from the list of all nodes that contain the UUID of
     * a node as its key and the corresponding IServiceIdentifier as its value.
     * 
     * @param entrySet   set containing all nodes
     * @param fileID     the file ID that is used to compute the responsible nodes
     * @param redundancy the number of times that the files should be duplicated
     */
    public RendezvousList(Set<Entry<String, IServiceIdentifier>> entrySet, FileID fileID, int redundancy) {
        for (Entry<String, IServiceIdentifier> entry : entrySet) {
            String hash = Hashing.sha256(fileID.getId() + entry.getKey());
            elements.put(hash, entry);
            if (elements.size() > redundancy) {
                elements.remove(elements.lastKey());
            }
        }
    }

    public List<String> uuidList() {
        List<String> result = new ArrayList<>();
        for (Entry<String, Entry<String, IServiceIdentifier>> entry : elements.entrySet()) {
            result.add(entry.getValue().getKey());
        }

        return result;
    }

    public List<IServiceIdentifier> serviceIDList() {
        List<IServiceIdentifier> result = new ArrayList<>();
        for (Entry<String, Entry<String, IServiceIdentifier>> entry : elements.entrySet()) {
            result.add(entry.getValue().getValue());
        }

        return result;
    }

    public String firstUUID() {
        return elements.firstEntry().getValue().getKey();
    }

    public String lastUUID() {
        return elements.lastEntry().getValue().getKey();
    }

    public IServiceIdentifier firstServiceID() {
        return elements.firstEntry().getValue().getValue();
    }

    public IServiceIdentifier lastServiceID() {
        return elements.lastEntry().getValue().getValue();
    }
}
