package eu.roebert.iufs.model;

import jadex.commons.transformation.annotations.IncludeFields;

/**
 * This class is responsible for giving files an unique identification.
 */
@IncludeFields(includePrivate = true)
public class FileID {
    private String id;

    public FileID() {
    }

    /**
     * Creates a new unique file identification.
     * 
     * @param id Unique file identification
     */
    public FileID(String id) {
        this.id = id;
    }

    /**
     * Return the unique file identification.
     * 
     * @return Unique file identification
     */
    public String getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof FileID) {
            FileID id = (FileID) o;

            return this.getId().equals(id.getId());
        }

        return false;
    }

    @Override
    public String toString() {
        return id;
    }
}
