package eu.roebert.iufs.model;

import java.security.PublicKey;

import org.cadeia.blockchain.crypto.Crypto;
import org.cadeia.blockchain.crypto.CryptoException;
import org.cadeia.blockchain.crypto.HexUtil;

import jadex.commons.transformation.annotations.IncludeFields;

/**
 * This class saves the compressed public key to send it with Jadex.
 */
@IncludeFields(includePrivate = true)
public class PubKey {
    private String compressedKey;

    /**
     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
     */
    public PubKey() {
    }

    /**
     * Wraps a public key to send it with Jadex.
     * 
     * @param key public key
     * @throws CryptoException if the key is invalid
     */
    public PubKey(PublicKey key) throws CryptoException {
        compressedKey = HexUtil.printHexBinary(Crypto.compressedKey(key));
    }

    /**
     * Wraps an already compressed public key to send it with Jadex.
     * 
     * @param key compressed key
     */
    public PubKey(String compressedKey) {
        this.compressedKey = compressedKey;
    }

    public String getCompressedKey() {
        return compressedKey;
    }

    public PublicKey getKey() throws CryptoException {
        return Crypto.parseCompressedPublicKey(HexUtil.parseHexBinary(compressedKey));
    }

    public boolean equals(Object o) {
        if (o instanceof PubKey) {
            return compressedKey.equals(((PubKey) o).getCompressedKey());
        }
        return false;
    }
}
