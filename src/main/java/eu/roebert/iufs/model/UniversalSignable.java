package eu.roebert.iufs.model;

import java.io.IOException;
import java.io.OutputStream;

import org.cadeia.blockchain.crypto.Signable;
import org.cadeia.blockchain.crypto.Signature;
import org.cadeia.blockchain.helper.JadexByteSerialize;

import jadex.commons.transformation.annotations.IncludeFields;

@IncludeFields(includePrivate = true)
public class UniversalSignable<T> implements Signable {
    private Signature signature;
    private PubKey signatory;
    private T object;

    /**
     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
     */
    public UniversalSignable() {
    }

    /**
     * Converts an object to a Signable object.
     * 
     * @param object object that should be converted
     */
    public UniversalSignable(T object) {
        this.object = object;
    }

    /**
     * @return Returns the origin object
     */
    public T getObject() {
        return object;
    }

    /**
     * Set the signatory of this object.
     * 
     * @param signatory of this object
     */
    public void setSignatory(PubKey signatory) {
        this.signatory = signatory;
    }

    /**
     * @return Returns the signatory of this object
     */
    public PubKey getSignatory() {
        return signatory;
    }

    @Override
    public Signature getSignature() {
        return signature;
    }

    @Override
    public void setSignature(Signature signature) {
        this.signature = signature;
    }

    @Override
    public void writeFieldsTo(OutputStream outstream) throws IOException {
        outstream.write(JadexByteSerialize.serialize(object));
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UniversalSignable<?>) {
            UniversalSignable<?> o2 = (UniversalSignable<?>) obj;

            return o2.getSignature().equals(getSignature()) && o2.getSignatory().equals(getSignatory());
        }

        return false;
    }
}
