package eu.roebert.iufs.model;

import jadex.commons.transformation.annotations.IncludeFields;

@IncludeFields(includePrivate = true)
public class Event<T> {
    private T data;

    /**
     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
     */
    public Event() {
    }

    /**
     * Creates a new event with data.
     * 
     * @param data event data
     */
    public Event(T data) {
        this.data = data;
    }

    /**
     * @return Returns the event data
     */
    public T getData() {
        return data;
    }
}
