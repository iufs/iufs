package eu.roebert.iufs.properties;

import jadex.commons.future.IFuture;

public interface IPropertiesParser {
    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     */
    public IFuture<String> get(String key);

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  byte
     */
    public IFuture<Byte> getByte(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  short
     */
    public IFuture<Short> getShort(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable int
     */
    public IFuture<Integer> getInt(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  long
     */
    public IFuture<Long> getLong(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  char
     */
    public IFuture<Character> getChar(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  float
     */
    public IFuture<Float> getFloat(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  double
     */
    public IFuture<Double> getDouble(String key) throws IllegalArgumentException;

    /**
     * Returns the value to which the specified key is mapped, or default value if
     * it is set and the properties contains no mapping for the key, otherwise null.
     * 
     * @param key The key whose associated value is to be returned
     * @return The value to which the specified key is mapped, or null if this map
     *         contains no mapping for the key
     * @throws IllegalArgumentException if the value does not contain a parsable
     *                                  boolean
     */
    public IFuture<Boolean> getBool(String key) throws IllegalArgumentException;
}
