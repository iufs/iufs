package eu.roebert.iufs.properties;

import java.util.Set;

import eu.roebert.iufs.exceptions.PropertiesException;
import jadex.bridge.service.annotation.Service;

/**
 * This Interface is responsible for store, retrieve and change application
 * settings.
 */
@Service
public interface IPropertiesService extends IPropertiesParser {
    /**
     * Associates the specified value with the specified key. If the properties
     * previously contained a mapping for the key, the old value is replaced.
     * 
     * @param key   Key with which the specified value is to be associated
     * @param value Value to be associated with the specified key
     * @throws PropertiesException If an I/O error occurs
     */
    public void store(String key, String value) throws PropertiesException;

    /**
     * Returns true if the properties contains a mapping for the specified key.
     * 
     * @param key The key whose presence in the properties is to be tested
     * @return true if the properties contains a mapping for the specified key
     */
    public boolean exists(String key);

    /**
     * This method only adds values, when key doesn't exists.
     * 
     * @param key   Key with which the specified value is to be associated
     * @param value Value to be associated with the specified key
     * @throws PropertiesException If an I/O error occurs
     */
    public void setIfNotExists(String key, String value) throws PropertiesException;

    /**
     * Returns a set of keys in this property list where the key and its
     * corresponding value are strings, including distinct keys in the default
     * property list if a key of the same name has not already been found from the
     * main properties list. Properties whose key or value is not of type String are
     * omitted. The returned set is not backed by the Properties object. Changes to
     * this Properties are not reflected in the set, or vice versa.
     * 
     * @return a set of keys in this property list where the key and its
     *         corresponding value are strings, including the keys in the default
     *         property list.
     */
    public Set<String> stringPropertyNames();

    /**
     * @return Returns a mocked properties object, that refers to this service.
     */
    public MockedProperties getMockedObject();
}
