package eu.roebert.iufs.properties;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Set;

import eu.roebert.iufs.exceptions.PropertiesException;

/**
 * This class can used like a normal {@link java.util.Properties Properties}
 * object, but it pass all calls to the {@link #serviceReference}.
 * 
 * <p>
 * It is not possible to use any I/O method from the {@link java.util.Properties
 * Properties} class or any method from the {@link java.util.Hashtable
 * Hashtable} class, because the {@link #serviceReference} is responsible for
 * all I/O-Operations.
 * </p>
 */
public class MockedProperties extends Properties {
    private static final long serialVersionUID = 2446064555649351692L;
    private IPropertiesService serviceReference;

    public MockedProperties() {
    }

    public MockedProperties(IPropertiesService serviceReference) {
        setServiceReference(serviceReference);
    }

    public IPropertiesService getServiceReference() {
        return serviceReference;
    }

    public void setServiceReference(IPropertiesService serviceReference) {
        this.serviceReference = serviceReference;
    }

    @Override
    public String getProperty(String key) {
        return serviceReference.get(key).get();
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        if (serviceReference.exists(key)) {
            return serviceReference.get(key).get();
        }

        return defaultValue;
    }

    @Override
    public void list(PrintStream out) {
    }

    @Override
    public void list(PrintWriter out) {
    }

    @Override
    public void load(InputStream is) {
    }

    @Override
    public void load(Reader reader) {
    }

    @Override
    public void loadFromXML(InputStream in) {
    }

    @Override
    public void save(OutputStream out, String comments) {
    }

    @Override
    public Object setProperty(String key, String value) {
        try {
            serviceReference.store(key, value);
        } catch (PropertiesException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void store(OutputStream out, String comments) {
    }

    @Override
    public void store(Writer writer, String comments) {
    }

    @Override
    public void storeToXML(OutputStream os, String comments) {
    }

    @Override
    public void storeToXML(OutputStream os, String comments, String encoding) {
    }

    @Override
    public Set<String> stringPropertyNames() {
        return serviceReference.stringPropertyNames();
    }

    @Override
    public Enumeration<String> propertyNames() {
        return java.util.Collections.enumeration(stringPropertyNames());
    }
}
