package eu.roebert.iufs.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.exceptions.PropertiesException;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;

/**
 * This Agent implements the PropertiesService and saves the files directly onto
 * the file system.
 */
@Agent
@Service
@ProvidedServices({
        @ProvidedService(name = "propertiesservice", type = IPropertiesService.class, scope = Binding.SCOPE_PLATFORM) })
public class PropertiesFileSystemAgent extends PropertiesParser implements IPropertiesService {
    private static final Logger LOG = LoggerFactory.getLogger(PropertiesFileSystemAgent.class);
    private Properties props;
    private String location;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    public IArgumentsResultsFeature arguments;

    public PropertiesFileSystemAgent() {
    }

    /**
     * This method adds default values to the properties object.
     */
    private void setDefaultValues() {
        try {
            // IUFS settings
            setIfNotExists("networkName", "iufs");
            setIfNotExists("networkPassword", "72c43c57-c79");
            setIfNotExists("uuid", UUID.randomUUID().toString());
            setIfNotExists("rootDir", "files");
            setIfNotExists("bufferSize", "65536"); // Tests have shown this buffer size is the optimum
            setIfNotExists("globalFileExpire.enabled", "false"); // Does not effect local settings in file flags
            setIfNotExists("globalFileExpire.CronExpression", "0 0 0/6 ? * * *"); // Check every six hours for expired
                                                                                  // files
            setIfNotExists("globalFileExpire.expireAfterDays", "30"); // a file expires if it has not been visited for n
                                                                      // days
            setIfNotExists("keyPairPath", location + "/keyPair.json");
            setIfNotExists("fullNode", "true"); // A full node also saves and serves files

            setIfNotExists("avDBPath", "db/av.db"); // The path to the av database
            setIfNotExists("avLimit", "3"); // Set the limit of infected files, a node can send, before he is blocked
                                            // permanently

            // ClamAV settings
            setIfNotExists("clamAVHost", "localhost");
            setIfNotExists("clamAVPort", "3310");
            setIfNotExists("clamAVEnabled", "true");

            // Quartz scheduler settings
            setIfNotExists("quartz.scheduler.startDelay", "10"); // give Jadex some time to do things
            setIfNotExists("org.quartz.scheduler.instanceName", "DefaultQuartzScheduler");
            setIfNotExists("org.quartz.scheduler.rmi.export", "false");
            setIfNotExists("org.quartz.scheduler.rmi.proxy", "false");
            setIfNotExists("org.quartz.scheduler.wrapJobExecutionInUserTransaction", "false");
            setIfNotExists("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
            setIfNotExists("org.quartz.threadPool.threadCount", "10");
            setIfNotExists("org.quartz.threadPool.threadPriority", "5");
            setIfNotExists("org.quartz.threadPool.threadsInheritContextClassLoaderOfInitializingThread", "true");
            setIfNotExists("org.quartz.jobStore.misfireThreshold", "60000");
            setIfNotExists("org.quartz.jobStore.class", "org.quartz.simpl.RAMJobStore");
            setIfNotExists("org.quartz.scheduler.skipUpdateCheck", "true");
            setIfNotExists("org.quartz.scheduler.jobFactory.class", "org.quartz.simpl.SimpleJobFactory");

            // File database
            setIfNotExists("fileDBPath", "db/files.db");

            // Number of nodes that a file should be stored on. Must be at least 2.
            setIfNotExists("redundancy", "2");

        } catch (PropertiesException e) {
            LOG.error("IOException was thrown.");
            e.printStackTrace();
        }
    }

    @AgentCreated
    public void created() throws PropertiesException {
        LOG.debug("Agent created");
        location = (String) arguments.getArguments().get("location");
        new File(location + "/").mkdirs();
        File dest = new File(location + "/iufs.properties");
        props = new Properties();

        if (dest.exists()) {
            try (InputStream is = new FileInputStream(dest)) {
                props.load(is);
            } catch (IOException e) {
                e.printStackTrace();
                throw new PropertiesException("Can't initialize properties service.");
            }
        }

        setDefaultValues();
    }

    @Override
    public void store(String key, String value) throws PropertiesException {
        try (OutputStream out = new FileOutputStream(new File(location + "/iufs.properties"))) {
            props.setProperty(key, value);
            props.store(out, "Settings for InterUniversal File System");
        } catch (IOException e) {
            e.printStackTrace();
            throw new PropertiesException();
        }
    }

    @Override
    public IFuture<String> get(String key) {
        return new Future<>(props.getProperty(key));
    }

    @Override
    public boolean exists(String key) {
        return props.containsKey(key);
    }

    @Override
    public void setIfNotExists(String key, String value) throws PropertiesException {
        if (!exists(key)) {
            store(key, value);
        }
    }

    @Override
    public MockedProperties getMockedObject() {
        return new MockedProperties(this);
    }

    @Override
    public Set<String> stringPropertyNames() {
        return props.stringPropertyNames();
    }
}
