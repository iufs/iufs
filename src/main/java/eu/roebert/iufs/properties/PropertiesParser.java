package eu.roebert.iufs.properties;

import jadex.commons.future.Future;
import jadex.commons.future.IFuture;

public abstract class PropertiesParser implements IPropertiesParser {
    @Override
    public IFuture<Byte> getByte(String key) throws IllegalArgumentException {
        return new Future<>(Byte.valueOf(get(key).get()));
    }

    @Override
    public IFuture<Short> getShort(String key) throws IllegalArgumentException {
        return new Future<>(Short.valueOf(get(key).get()));
    }

    @Override
    public IFuture<Integer> getInt(String key) throws IllegalArgumentException {
        return new Future<>(Integer.valueOf(get(key).get()));
    }

    @Override
    public IFuture<Long> getLong(String key) throws IllegalArgumentException {
        return new Future<>(Long.valueOf(get(key).get()));
    }

    @Override
    public IFuture<Character> getChar(String key) throws IllegalArgumentException {
        return new Future<>(get(key).get().charAt(0));
    }

    @Override
    public IFuture<Float> getFloat(String key) throws IllegalArgumentException {
        return new Future<>(Float.valueOf(get(key).get()));
    }

    @Override
    public IFuture<Double> getDouble(String key) throws IllegalArgumentException {
        return new Future<>(Double.valueOf(get(key).get()));
    }

    @Override
    public IFuture<Boolean> getBool(String key) throws IllegalArgumentException {
        return new Future<>(Boolean.valueOf(get(key).get()));
    }

}
