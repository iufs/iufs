package eu.roebert.iufs.compression;

import java.io.File;
import java.io.IOException;

public interface Compression {
    /**
     * Compresses the source file and save it under the dest file path.
     * 
     * @param source source file
     * @param dest   destination file
     */
    public void compress(File source, File dest) throws IOException;

    /**
     * Decompresses the source file and save it under the dest file path.
     * 
     * @param source source file
     * @param dest   destination file
     */
    public void decompress(File source, File dest) throws IOException;
}
