package eu.roebert.iufs.compression;

public class CompressionFactory {
    public static Compression get(String algorithm) {
        switch (algorithm) {
        case "lz4":
            return new LZ4Compression();
        default:
            return new NoCompression();
        }
    }
}
