package eu.roebert.iufs.compression;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import net.jpountz.lz4.LZ4BlockInputStream;
import net.jpountz.lz4.LZ4BlockOutputStream;

/**
 * This file is based on the code from:
 * https://github.com/lz4/lz4-java/issues/100#issuecomment-492263805
 * 
 * (De-)compress a source file in/from LZ4 to the dest location.
 */
public class LZ4Compression implements Compression {
    public static final int BUFFER_SIZE = 8096;

    @Override
    public void compress(File source, File dest) throws IOException {
        InputStream directIn = null;
        BufferedInputStream bufferedIn = null;
        OutputStream directOut = null;
        BufferedOutputStream bufferedOut = null;
        LZ4BlockOutputStream outStream = null;

        try {
            Path compressedPath = Paths.get(dest.getAbsolutePath());
            if (!Files.exists(compressedPath)) {
                Files.createFile(compressedPath);
            }

            directIn = Files.newInputStream(Paths.get(source.getAbsolutePath()));
            bufferedIn = new BufferedInputStream(directIn);
            directOut = Files.newOutputStream(compressedPath);
            bufferedOut = new BufferedOutputStream(directOut);
            outStream = new LZ4BlockOutputStream(bufferedOut);

            final byte[] buffer = new byte[BUFFER_SIZE * 16];
            int readBytes = 0;
            do {
                readBytes = bufferedIn.read(buffer);
                if (readBytes > 0) {
                    outStream.write(buffer, 0, readBytes);
                }
            } while (readBytes != -1);
        } finally {
            if (outStream != null) {
                outStream.close();
            }
            if (bufferedOut != null) {
                bufferedOut.close();
            }
            if (directOut != null) {
                directOut.close();
            }
            if (bufferedIn != null) {
                bufferedIn.close();
            }
            if (directIn != null) {
                directIn.close();
            }
        }
    }

    @Override
    public void decompress(File source, File dest) throws IOException {
        InputStream directIn = null;
        BufferedInputStream bufferedIn = null;
        OutputStream directOut = null;
        BufferedOutputStream bufferedOut = null;
        LZ4BlockInputStream inStream = null;

        try {
            Path decompressedPath = Paths.get(dest.getAbsolutePath());
            if (!Files.exists(decompressedPath)) {
                Files.createFile(decompressedPath);
            }
            directIn = Files.newInputStream(Paths.get(source.getAbsolutePath()));
            bufferedIn = new BufferedInputStream(directIn);
            inStream = new LZ4BlockInputStream(bufferedIn);
            directOut = Files.newOutputStream(decompressedPath);
            bufferedOut = new BufferedOutputStream(directOut);

            final byte[] buffer = new byte[BUFFER_SIZE * 16];
            int readBytes = 0;
            do {
                readBytes = inStream.read(buffer);
                if (readBytes > 0) {
                    bufferedOut.write(buffer, 0, readBytes);
                }
            } while (readBytes != -1);
        } finally {
            if (bufferedOut != null) {
                bufferedOut.close();
            }
            if (directOut != null) {
                directOut.close();
            }
            if (inStream != null) {
                inStream.close();
            }
            if (bufferedIn != null) {
                bufferedIn.close();
            }
            if (directIn != null) {
                directIn.close();
            }
        }
    }
}
