package eu.roebert.iufs.compression;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;

public class NoCompression implements Compression {

    @Override
    public void compress(File source, File dest) throws IOException {
        FileUtils.copyFile(source, dest);
    }

    @Override
    public void decompress(File source, File dest) throws IOException {
        FileUtils.copyFile(source, dest);
    }

}
