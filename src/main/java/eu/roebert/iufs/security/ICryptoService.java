package eu.roebert.iufs.security;

import org.cadeia.blockchain.crypto.CryptoException;
import org.cadeia.blockchain.crypto.Signable;

import eu.roebert.iufs.common.helper.CryptoConsumer;
import eu.roebert.iufs.model.PubKey;
import jadex.bridge.service.annotation.Service;
import jadex.commons.future.IFuture;

/**
 * This service provides access to the public/private key pair of the IUFS node
 * and also to some methods for signing.
 */
@Service
public interface ICryptoService {
    /**
     * @return Returns the public key as PubKey object.
     * @throws CryptoException on failure
     */
    public IFuture<PubKey> getPublicKey() throws CryptoException;

    /**
     * Signs the given signable with the private key of this IUFS node. This will
     * put the resulting signature into a new signable object, that will be
     * returned.
     * 
     * @param signable signature to create
     * @return the signed object
     * @throws CryptoException on failure
     */
    public <T extends Signable> IFuture<T> sign(T signable) throws CryptoException;

    /**
     * Signs the given signable with the private key of this IUFS node. This will
     * put the resulting signature into a the signable object.
     * 
     * @return a consumer that accept an Signable
     * @throws CryptoException on failure
     */
    public CryptoConsumer<Signable> sign() throws CryptoException;

    /**
     * Verify the signature of the given content.
     * 
     * @param publicKey public key of the signatory
     * @param content   signable object that should be checked
     * @throws CryptoException on failure
     * @return Whether the signature is valid or not
     */
    public IFuture<Boolean> verifySignature(PubKey publicKey, Signable content) throws CryptoException;
}
