package eu.roebert.iufs.security;

import java.io.File;
import java.io.IOException;
import java.security.KeyPair;
import java.util.Objects;

import org.cadeia.blockchain.api.exception.KeypairJsonNotFound;
import org.cadeia.blockchain.crypto.Crypto;
import org.cadeia.blockchain.crypto.CryptoException;
import org.cadeia.blockchain.crypto.Signable;
import org.cadeia.blockchain.helper.JsonReaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.common.helper.CryptoConsumer;
import eu.roebert.iufs.common.helper.JsonWriterUtil;
import eu.roebert.iufs.model.PubKey;
import eu.roebert.iufs.properties.IPropertiesService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@ProvidedServices({ @ProvidedService(name = "cryptoservice", type = ICryptoService.class) })
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class CryptoAgent implements ICryptoService {
    private static final Logger LOG = LoggerFactory.getLogger(CryptoAgent.class);
    private KeyPair keyPair;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    private IRequiredServicesFeature requiredServices;

    /**
     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
     */
    public CryptoAgent() {
    }

    @AgentCreated
    public void created() {
        init();
        LOG.info("{} agent created.", this);
    }

    @Override
    public IFuture<PubKey> getPublicKey() throws CryptoException {
        return new Future<PubKey>(new PubKey(keyPair.getPublic()));
    }

    @Override
    public <T extends Signable> IFuture<T> sign(T signable) throws CryptoException {
        Objects.requireNonNull(signable);
        Objects.requireNonNull(keyPair.getPrivate());
        Crypto.sign(keyPair.getPrivate(), signable);
        Objects.requireNonNull(signable.getSignature());

        return new Future<T>(signable);
    }

    @Override
    public CryptoConsumer<Signable> sign() throws CryptoException {
        return (Signable x) -> {
            Crypto.sign(keyPair.getPrivate(), x);
        };
    }

    @Override
    public IFuture<Boolean> verifySignature(PubKey publicKey, Signable content) throws CryptoException {
        Objects.requireNonNull(publicKey.getKey());
        Objects.requireNonNull(content);
        return new Future<Boolean>(Crypto.verifySignature(publicKey.getKey(), content));
    }

    private void init() {
        String path = propertiesservice.get("keyPairPath").get();
        Objects.requireNonNull(path);

        if (!new File(path).exists()) {
            keyPair = Crypto.generateKeys();
            try {
                JsonWriterUtil.writeKeyPair(keyPair, path);
            } catch (IOException e) {
                LOG.error("Couldn't store keys: " + e.getMessage());
                System.exit(0);
            }
        } else {
            try {
                keyPair = JsonReaderUtil.readKeypair(path);
            } catch (KeypairJsonNotFound e) {
                LOG.error("Couldn't found a key pair: " + e.getMessage());
                System.exit(0);
            }
        }
    }
}
