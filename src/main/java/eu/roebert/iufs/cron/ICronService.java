package eu.roebert.iufs.cron;

import java.text.ParseException;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.SchedulerException;

import jadex.bridge.service.annotation.Service;
import jadex.commons.future.IFuture;

/**
 * @note every implementation of this interface should make the
 *       {@link jadex.bridge.service.component.IRequiredServicesFeature
 *       IRequiredServicesFeature} available for all jobs. This can be achieved
 *       when it is passed to the
 *       {@link org.quartz.SchedulerContext#put(String, Object) put} method.
 */
@Service
public interface ICronService {

    /**
     * Submit a job to the scheduler, that will be execute as soon as possible.
     * 
     * @param job class that should be executed
     * @return a future that is already done
     * @throws SchedulerException if there is an internal Scheduler error, or if the
     *                            Job is not durable, or a Job with the same name
     *                            already exists, and <code>replace</code> is
     *                            <code>false</code>.
     */
    public IFuture<Void> submitJob(Class<? extends Job> job) throws SchedulerException;

    /**
     * Submit a job to the scheduler, that will be execute as soon as possible.
     * 
     * @param job  class that should be executed
     * @param data holds state information for {@code job} instance
     * @return a future that is already done
     * @throws SchedulerException if there is an internal Scheduler error, or if the
     *                            Job is not durable, or a Job with the same name
     *                            already exists, and <code>replace</code> is
     *                            <code>false</code>.
     * @throws SchedulerException if there is an internal Scheduler error, or if the
     *                            Job is not durable, or a Job with the same name
     *                            already exists, and <code>replace</code> is
     *                            <code>false</code>.
     */
    public IFuture<Void> submitJob(Class<? extends Job> job, JobDataMap data) throws SchedulerException;

    /**
     * Submit a job to the scheduler, that will be execute on cronExpression.
     * 
     * @param job            class that should be executed
     * @param cronExpression when the job should be executed
     * @return a future that is already done
     * @throws java.text.ParseException if the string expression cannot be parsed
     *                                  into a valid
     *                                  {@link org.quartz.CronExpression
     *                                  CronExpression}
     * @throws SchedulerException       if there is an internal Scheduler error, or
     *                                  if the Job is not durable, or a Job with the
     *                                  same name already exists, and
     *                                  <code>replace</code> is <code>false</code>.
     */
    public IFuture<Void> submitJob(Class<? extends Job> job, String cronExpression)
            throws ParseException, SchedulerException;

    /**
     * Submit a job to the scheduler, that will be execute on cronExpression.
     * 
     * @param job            class that should be executed
     * @param cronExpression when the job should be executed
     * @param data           holds state information for {@code job} instance
     * @return a future that is already done
     * @throws java.text.ParseException if the string expression cannot be parsed
     *                                  into a valid
     *                                  {@link org.quartz.CronExpression
     *                                  CronExpression}
     * @throws SchedulerException       if there is an internal Scheduler error, or
     *                                  if the Job is not durable, or a Job with the
     *                                  same name already exists, and
     *                                  <code>replace</code> is <code>false</code>.
     */
    public IFuture<Void> submitJob(Class<? extends Job> job, String cronExpression, JobDataMap data)
            throws ParseException, SchedulerException;

    /**
     * Submit a job to the scheduler, that will be execute on cronExpression.
     * <b>Jobs with same {@code key} will be overridden.</b>
     * 
     * @param job            class that should be executed
     * @param cronExpression when the job should be executed
     * @param data           holds state information for {@code job} instance
     * @param key            key that identifies the job in the scheduler
     * @return a future that is already done
     * @throws java.text.ParseException if the string expression cannot be parsed
     *                                  into a valid
     *                                  {@link org.quartz.CronExpression
     *                                  CronExpression}
     * @throws SchedulerException       if there is an internal Scheduler error, or
     *                                  if the Job is not durable, or a Job with the
     *                                  same name already exists, and
     *                                  <code>replace</code> is <code>false</code>.
     */
    public IFuture<Void> submitJob(Class<? extends Job> job, String cronExpression, JobDataMap data, String key)
            throws ParseException, SchedulerException;

    /**
     * Adds a reference to a Jadex service to the global scheduler context. Must be
     * set on every application startup again, otherwise the reference is not
     * available.
     * 
     * @param service Jadex service that should be added
     * @param key     the key under the service can be access
     * @param replace if a service or key is already stored, override?
     * @throws SchedulerException if there is an internal Scheduler error, or if the
     *                            Job is not durable, or a Job with the same name
     *                            already exists, and <code>replace</code> is
     *                            <code>false</code>.
     * @return a future that is already done
     */
    public IFuture<Void> addReferenceToService(Object service, String key, boolean replace) throws SchedulerException;
}
