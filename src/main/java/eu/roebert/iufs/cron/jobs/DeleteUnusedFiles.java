package eu.roebert.iufs.cron.jobs;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;

import org.apache.commons.io.FilenameUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerContext;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.filedb.IFileDBService;
import eu.roebert.iufs.io.IOService;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.properties.IPropertiesService;
import jadex.bridge.service.component.IRequiredServicesFeature;

/**
 * This job searches at regular intervals for unused files that can be deleted.
 */
@DisallowConcurrentExecution
public class DeleteUnusedFiles implements Job {
    private static final Logger log = LoggerFactory.getLogger(DeleteUnusedFiles.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.debug("JOB execution started for: {}", context.getJobDetail().getKey().getName());
        SchedulerContext schedulerContext = null;
        try {
            schedulerContext = context.getScheduler().getContext();
        } catch (SchedulerException e1) {
            e1.printStackTrace();
        }

        IRequiredServicesFeature requiredServices = (IRequiredServicesFeature) schedulerContext.get("requiredServices");
        IPropertiesService props = (IPropertiesService) requiredServices.getRequiredService("propertiesservice").get();
        IFileDBService filedbservice = (IFileDBService) schedulerContext.get("filedbservice");
        IOService io = (IOService) schedulerContext.get("ioservice");

        boolean globalExpire = props.getBool("globalFileExpire.enabled").get();
        int globalExpireDays = Math.max(0, props.getInt("globalFileExpire.expireAfterDays").get());

        File dir = new File(props.get("rootDir").get() + "/");

        try {
            Files.find(dir.toPath(), Integer.MAX_VALUE,
                    (path, attr) -> attr.isRegularFile() && path.getFileName().toString().endsWith(".iufs"))
                    .forEach((file) -> {
                        FileID fid = new FileID(FilenameUtils.removeExtension(file.getFileName().toString()));

                        long lastAccess = Long.valueOf(
                                filedbservice.getFlag(fid, "lastAccess", String.valueOf(System.currentTimeMillis())).get());
                        boolean expire = Boolean.valueOf(filedbservice.getFlag(fid, "expire", String.valueOf(globalExpire)).get());
                        int expireDays = Integer
                                .valueOf(filedbservice.getFlag(fid, "expireDays", String.valueOf(globalExpireDays)).get());

                        if (expire && (System.currentTimeMillis() - lastAccess) > (86400000 * expireDays)) {
                            if(io.delete(fid).get()) {
                                log.info("The scheduler deleted the unused file {}.", file.toAbsolutePath());
                            } else {
                                log.error("The scheduler could not delete the unused file {}.", file.toAbsolutePath());
                            }
                        }
                    });
        } catch (NoSuchFileException e) {
            log.info("The \"file\" folder could not be found. Is IUFS already set up?");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
