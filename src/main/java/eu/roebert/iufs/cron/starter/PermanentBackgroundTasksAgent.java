package eu.roebert.iufs.cron.starter;

import org.quartz.JobDataMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.cron.ICronService;
import eu.roebert.iufs.cron.jobs.DeleteUnusedFiles;
import eu.roebert.iufs.filedb.IFileDBService;
import eu.roebert.iufs.io.IOService;
import eu.roebert.iufs.properties.IPropertiesService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

/**
 * This agent register all permanent background task on the scheduler.
 */
@Agent
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "cronservice", type = ICronService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "ioservice", type = IOService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "filedbservice", type = IFileDBService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class PermanentBackgroundTasksAgent {
    private static final Logger log = LoggerFactory.getLogger(PermanentBackgroundTasksAgent.class);

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @AgentService(lazy = false)
    ICronService cronservice;

    @AgentService(lazy = false)
    IOService ioservice;

    @AgentService(lazy = false)
    IFileDBService filedbservice;

    @AgentFeature
    private IRequiredServicesFeature requiredServices;

    @Agent
    private IInternalAccess agent;

    public PermanentBackgroundTasksAgent() {
    }

    @AgentCreated
    public void created() {
        log.debug("Agent created");

        addRequiredServices();
        addTasks();
    }

    /**
     * Register all required services for the tasks.
     */
    private void addRequiredServices() {
        try {
            cronservice.addReferenceToService(ioservice, "ioservice", true);
            cronservice.addReferenceToService(filedbservice, "filedbservice", true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Register all tasks.
     */
    private void addTasks() {
        try {
            cronservice.submitJob(DeleteUnusedFiles.class,
                    propertiesservice.get("globalFileExpire.CronExpression").get(), new JobDataMap(),
                    "DeleteUnusedFilesTask");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
