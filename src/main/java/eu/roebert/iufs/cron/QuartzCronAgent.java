package eu.roebert.iufs.cron;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

import java.text.ParseException;
import java.util.UUID;

import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.properties.MockedProperties;
import eu.roebert.iufs.cron.listener.SchedulerStartedListener;
import eu.roebert.iufs.properties.IPropertiesService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

/**
 * @note To run this agent with a database it is necessary that the user change
 *       some values in the <b>IUFS properties</b> file. For further information
 *       please have a look at the Quartz documentation:
 * 
 *       <a href=
 *       "https://github.com/quartz-scheduler/quartz/blob/master/docs/configuration.adoc">Quartz
 *       Configuration Reference</a>.
 * 
 *       You also have to create a table with this script: <a href=
 *       "https://github.com/elventear/quartz-scheduler/blob/master/distribution/src/main/assembly/root/docs/dbTables/tables_mysql_innodb.sql">tables_mysql_innodb.sql</a>
 * 
 *       The required properties are:
 *       <ul>
 *       <li><code>org.quartz.jobStore.class=org.quartz.impl.jdbcjobstore.JobStoreTX</code>
 *       <b>Override the old value!</b></li>
 *       <li><code>org.quartz.jobStore.tablePrefix=QRTZ_</code></li>
 *       <li><code>org.quartz.jobStore.driverDelegateClass=org.quartz.impl.jdbcjobstore.StdJDBCDelegate</code></li>
 *       <li><code>org.quartz.jobStore.isClustered=false</code></li>
 *       <li><code>org.quartz.jobStore.dataSource=quartzDataSource</code></li>
 *       <li><code>org.quartz.dataSource.quartzDataSource.driver=com.mysql.cj.jdbc.Driver</code>
 *       for MySQL database</li>
 *       <li><code>org.quartz.dataSource.quartzDataSource.URL=jdbc:mysql://localhost:3306/quartz?useUnicode=true&amp;useJDBCCompliantTimezoneShift=true&amp;useLegacyDatetimeCode=false&amp;serverTimezone=UTC</code>
 *       here is <b>quartz</b> the database name</li>
 *       <li><code>org.quartz.dataSource.quartzDataSource.user=quartz</code></li>
 *       <li><code>org.quartz.dataSource.quartzDataSource.password=12345678</code></li>
 *       <li><code>org.quartz.dataSource.quartzDataSource.maxConnections=10</code></li>
 *       </ul>
 */
@Agent
@Service
@ProvidedServices({ @ProvidedService(name = "cronservice", type = ICronService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class QuartzCronAgent implements ICronService {
    private static final Logger log = LoggerFactory.getLogger(QuartzCronAgent.class);
    protected SchedulerFactory factory;
    protected Scheduler scheduler;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @Agent
    private IInternalAccess agent;

    @AgentFeature
    private IRequiredServicesFeature requiredServices;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    /**
     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
     */
    public QuartzCronAgent() {
    }

    @AgentCreated
    public void created() {
        log.info("{} agent created.", this);
        factory = new StdSchedulerFactory();
        int delay = propertiesservice.getInt("quartz.scheduler.startDelay").get();

        MockedProperties props = propertiesservice.getMockedObject();

        try {
            ((StdSchedulerFactory) factory).initialize(props);
            scheduler = factory.getScheduler();
            scheduler.getContext().put("requiredServices", requiredServices);
            scheduler.startDelayed(delay);

            log.info("Quartz scheduler will start in {} seconds.", delay);
        } catch (SchedulerException e) {
            log.error("Can't create the Quartz scheduler.");
            e.printStackTrace();
        }
    }

    @Override
    public IFuture<Void> submitJob(Class<? extends Job> job, JobDataMap data) throws SchedulerException {
        JobKey key = new JobKey(UUID.randomUUID().toString());
        JobDetail jobDet = newJob(job).setJobData(data).withIdentity(key).build();
        Trigger trigger = newTrigger().startNow().withSchedule(simpleSchedule().withMisfireHandlingInstructionFireNow())
                .forJob(key).build();
        scheduler.scheduleJob(jobDet, trigger);

        return Future.DONE;
    }

    @Override
    public IFuture<Void> submitJob(Class<? extends Job> job) throws SchedulerException {
        return submitJob(job, new JobDataMap());
    }

    @Override
    public IFuture<Void> submitJob(Class<? extends Job> job, String cronExpression)
            throws ParseException, SchedulerException {
        return submitJob(job, cronExpression, new JobDataMap());
    }

    @Override
    public IFuture<Void> submitJob(Class<? extends Job> job, String cronExpression, JobDataMap data)
            throws ParseException, SchedulerException {
        return submitJob(job, cronExpression, data, UUID.randomUUID().toString());
    }

    @Override
    public IFuture<Void> submitJob(Class<? extends Job> job, String cronExpression, JobDataMap data, String key)
            throws ParseException, SchedulerException {
        scheduler.getListenerManager().addSchedulerListener(new SchedulerStartedListener() {

            @Override
            public void schedulerStarted() {
                JobKey jobKey = JobKey.jobKey(key);

                try {
                    if (scheduler.unscheduleJob(TriggerKey.triggerKey(key)) & scheduler.deleteJob(jobKey)) {
                        log.info("Delete old job to replace with new one");
                    }

                    JobDetail jobDet = newJob(job).setJobData(data).withIdentity(key).build();
                    Trigger trigger = newTrigger().withIdentity(key)
                            .withSchedule(cronSchedule(cronExpression).withMisfireHandlingInstructionFireAndProceed())
                            .forJob(key).build();
                    scheduler.scheduleJob(jobDet, trigger);
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }

        });

        return Future.DONE;
    }

    @Override
    public IFuture<Void> addReferenceToService(Object service, String key, boolean replace) throws SchedulerException {

        if (scheduler.getContext().get(key) != null && replace) {
            scheduler.getContext().put(key, service);
        } else if (scheduler.getContext().get(key) == null) {
            scheduler.getContext().put(key, service);
        }

        return Future.DONE;
    }
}
