package eu.roebert.iufs.neighbours;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;

import org.cadeia.blockchain.crypto.CryptoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Optional;

import eu.roebert.iufs.filedb.IFileDBService;
import eu.roebert.iufs.future.ResultObjectListener;
import eu.roebert.iufs.io.IOService;
import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.IntRefHolder;
import eu.roebert.iufs.model.PubKey;
import eu.roebert.iufs.model.RendezvousList;
import eu.roebert.iufs.model.UniversalSignable;
import eu.roebert.iufs.properties.IPropertiesService;
import eu.roebert.iufs.security.ICryptoService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.ImmediateComponentStep;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.component.IExecutionFeature;
import jadex.bridge.service.IService;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.ServiceComponent;
import jadex.bridge.service.annotation.Timeout;
import jadex.bridge.service.component.IRequiredServicesFeature;
import jadex.bridge.service.search.SServiceProvider;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.commons.future.ISubscriptionIntermediateFuture;
import jadex.commons.future.SubscriptionIntermediateFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentKilled;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@RequiredServices({
        @RequiredService(name = "neighbourhoodservices", type = INeighbourhoodService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_GLOBAL)),
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "ioservice", type = IOService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "cryptoservice", type = ICryptoService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "filedbservice", type = IFileDBService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "self", type = INeighbourhoodService.class, binding = @Binding(scope = Binding.SCOPE_COMPONENT)) })
@ProvidedServices({ @ProvidedService(type = INeighbourhoodService.class, scope = Binding.SCOPE_GLOBAL) })
public class RendezvousHashingAgent implements INeighbourhoodService {
    private static final Logger log = LoggerFactory.getLogger(RendezvousHashingAgent.class);

    public static final String KEY_PEER_DISCOVERY_INITIAL_WAIT = "initial_wait";
    public static final String KEY_PEER_DISCOVERY_PERIODIC_WAIT = "periodic_wait";
    private static final int RED_DEFAULT = 2;
    private static final int RED_MINIMUM = 2;
    private static int redundancy = RED_DEFAULT;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @Agent
    IInternalAccess agent;

    @AgentService(lazy = false)
    IOService ioservice;

    @AgentService(lazy = false)
    ICryptoService cryptoservice;

    @AgentService(lazy = false)
    IFileDBService filedbservice;

    @AgentService(lazy = false)
    INeighbourhoodService self;

    @AgentFeature
    IRequiredServicesFeature requiredServices;

    @AgentFeature
    IExecutionFeature exe;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    @ServiceComponent
    private IInternalAccess agentAccess;

    private HashMap<String, IServiceIdentifier> nodes;
    private String myUUID;

    private IServiceIdentifier ownServiceID;

    public RendezvousHashingAgent() {
    }

    @AgentCreated
    public void onAgentCreated() {
        log.info("{} Agent Started", this);

        ownServiceID = ((IService) self).getServiceIdentifier();
        nodes = new HashMap<String, IServiceIdentifier>();
        myUUID = propertiesservice.get("uuid").get();

        Map<String, Object> args = arguments.getResults();
        long initial = (long) args.getOrDefault(KEY_PEER_DISCOVERY_INITIAL_WAIT, 300L);
        long timeout = (long) args.getOrDefault(KEY_PEER_DISCOVERY_PERIODIC_WAIT, 60000L);

        try {
            Optional<Integer> redundancyOp = Optional
                    .fromNullable(Integer.parseInt((String) arguments.getArguments().get("redundancy")));
            redundancy = redundancyOp.or(2);
        } catch (NumberFormatException e) {
            redundancy = RED_DEFAULT;
        }
        if (redundancy < RED_MINIMUM) {
            redundancy = RED_MINIMUM;
        }

        nodes.put(myUUID, ownServiceID);

        exe.repeatStep(initial, timeout, ia -> {
            updateNeighbourhood();
            return IFuture.DONE;
        });
    }

    /**
     * Notifies all nodes in the network that this node is now going offline.
     */
    @AgentKilled
    public void onAgentKilled() {
        log.info("Updating nodes of absence...");
        for (IServiceIdentifier sid : nodes.values()) {
            INeighbourhoodService service = (INeighbourhoodService) SServiceProvider.getService(agent, sid, true).get();
            service.removeNeighbour(myUUID);
        }
        log.info("Updating nodes of absence completed!");
    }

    /**
     * Updates the set of all nodes and checks for new and absent nodes. New nodes
     * will be notified of the presence of this node and lost nodes will be removed.
     * Iff there were no known neighbours before, all files that are already on the
     * system will be relocated to the responsible nodes.
     */
    private void updateNeighbourhood() {
        requiredServices.searchServices(INeighbourhoodService.class, Binding.SCOPE_GLOBAL).addResultListener(result -> {
            Collection<INeighbourhoodService> neighbours = (Collection<INeighbourhoodService>) result;
            Set<String> absentNeighbours = new HashSet<>(nodes.keySet());

            int nodesBefore = nodes.size();
            nodes = new HashMap<String, IServiceIdentifier>();
            nodes.put(myUUID, ownServiceID);

            for (INeighbourhoodService neighbour : neighbours) {
                if (neighbour.isFullNode().get()) {
                    String neighbourID = neighbour.getUUID().get();
                    nodes.put(neighbourID, neighbour.getServiceID().get());

                    if (!neighbour.addNeighbour(myUUID, ownServiceID).get()) {
                        absentNeighbours.remove(neighbourID);
                    } else if (!neighbourID.equals(myUUID)) {
                        log.info("Found new node with uuid: {}", neighbourID);
                    }
                }
            }

            if (nodesBefore == 1 && nodes.size() > 1) {
                propagateEarlyFiles();
            }

            for (String absentNeighbour : absentNeighbours) {
                log.info("Lost node with uuid: {}", absentNeighbour);
                removeNeighbour(absentNeighbour);
            }
        });

    }

    @Override
    public IFuture<IServiceIdentifier> getResponsibleNode(FileID fileID) {
        Random rand = new Random();

        List<IServiceIdentifier> respNodes = getResponsibleNodesSIDs(fileID).get();
        IServiceIdentifier neighbour = null;

        if (respNodes.size() > 0) {
            neighbour = respNodes.get(rand.nextInt(respNodes.size()));
        } else {
            return new Future<>((IServiceIdentifier) null);
        }

        return new Future<>(neighbour);
    }

    @Override
    public IFuture<RendezvousList> getResponsibleNodes(FileID fileID) {
        RendezvousList hrf = new RendezvousList(nodes.entrySet(), fileID, redundancy);
        log.info("Responsible nodes for file {}: {}", fileID, hrf.uuidList().toString());

        return new Future<>(hrf);
    }

    @Override
    public IFuture<List<String>> getResponsibleNodesUUIDs(FileID fileID) {
        return new Future<>(getResponsibleNodes(fileID).get().uuidList());
    }

    @Override
    public IFuture<List<IServiceIdentifier>> getResponsibleNodesSIDs(FileID fileID) {
        return new Future<>(getResponsibleNodes(fileID).get().serviceIDList());
    }

    @Override
    public IFuture<Boolean> isResponsible(String uuid, FileID fileID) {
        return new Future<>(getResponsibleNodesUUIDs(fileID).get().contains(uuid));
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<Void> propagateEarlyFiles() {
        updateNeighbourhood();
        exe.scheduleStep(new ImmediateComponentStep<Void>() {
            @Override
            public IFuture<Void> execute(IInternalAccess ia) {
                List<FileID> allFileIDs = filedbservice.getAllKeys().get();
                HashMap<FileID, IServiceIdentifier> relocationMap = new HashMap<FileID, IServiceIdentifier>();

                for (FileID fileID : allFileIDs) {
                    for (IServiceIdentifier sid : getResponsibleNodesSIDs(fileID).get()) {
                        if (!sid.equals(ownServiceID)) {
                            relocationMap.put(fileID, sid);
                        }
                    }
                }
                if (relocationMap.size() > 0) {
                    log.info("Relocating {} early files to new neighbour...", relocationMap.size());
                    relocate(relocationMap).addIntermediateResultListener(result -> {
                        FileID resultFileID = (FileID) result;
                        if (!isResponsible(myUUID, resultFileID).get()) {
                            ioservice.delete(resultFileID);
                        }
                    });
                }
                return Future.DONE;
            }
        });

        return Future.DONE;
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<Boolean> addNeighbour(String uuid, IServiceIdentifier sid) {
        Future<Boolean> rtn = new Future<Boolean>();

        if (nodes.put(uuid, sid) == null && !uuid.equals(myUUID)) {
            exe.scheduleStep(new ImmediateComponentStep<Void>() {
                @Override
                public IFuture<Void> execute(IInternalAccess ia) {
                    List<FileID> allFileIDs = filedbservice.getAllKeys().get();
                    HashMap<FileID, IServiceIdentifier> relocationMap = new HashMap<FileID, IServiceIdentifier>();

                    for (FileID fileID : allFileIDs) {
                        if (!isResponsible(myUUID, fileID).get() || nodes.size() <= redundancy) {
                            if (isResponsible(uuid, fileID).get()) {
                                relocationMap.put(fileID, sid);
                            } else {
                                ioservice.delete(fileID);
                            }
                        }
                    }
                    if (relocationMap.size() > 0) {
                        log.info("Relocating {} files to new neighbour...", relocationMap.size());
                        relocate(relocationMap).addIntermediateResultListener(result -> {
                            FileID resultFileID = (FileID) result;
                            if (!isResponsible(myUUID, resultFileID).get()) {
                                ioservice.delete(resultFileID);
                            }
                        });
                    }

                    rtn.setResult(true);

                    return Future.DONE;
                }
            });

            return rtn;
        }

        return Future.FALSE;
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<Void> removeNeighbour(String uuid) {
        if (!uuid.equals(myUUID) && nodes.containsKey(uuid)) {
            exe.scheduleStep(new ImmediateComponentStep<Void>() {
                @Override
                public IFuture<Void> execute(IInternalAccess ia) {
                    HashMap<FileID, IServiceIdentifier> relocationMap = new HashMap<FileID, IServiceIdentifier>();
                    List<FileID> allFileIDs = filedbservice.getAllKeys().get();

                    for (FileID fileID : allFileIDs) {
                        if (isResponsible(uuid, fileID).get()) {
                            nodes.remove(uuid);
                            RendezvousList respNodes = getResponsibleNodes(fileID).get();

                            if (myUUID.equals(respNodes.firstUUID())) {
                                IServiceIdentifier leastRespSID = respNodes.lastServiceID();
                                relocationMap.put(fileID, leastRespSID);
                            }
                        }
                    }
                    if (relocationMap.size() > 0) {
                        log.info("Relocating {} files to newly responsible nodes...", relocationMap.size());
                        relocate(relocationMap);
                    }

                    return Future.DONE;
                }
            });
        }

        return Future.DONE;
    }

    @Override
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<FileID> relocate(Map<FileID, IServiceIdentifier> relocationMap) {
        SubscriptionIntermediateFuture<FileID> rtn = new SubscriptionIntermediateFuture<>();

        exe.scheduleStep(new ImmediateComponentStep<Void>() {
            @Override
            public IFuture<Void> execute(IInternalAccess ia) {
                IntRefHolder nodeCalls = new IntRefHolder(0);

                for (Entry<FileID, IServiceIdentifier> entry : relocationMap.entrySet()) {
                    FileID fileID = entry.getKey();
                    IServiceIdentifier node = entry.getValue();

                    INeighbourhoodService service = (INeighbourhoodService) SServiceProvider
                            .getService(agent, node, true).get();

                    if (!node.equals(ownServiceID) && !service.fileExists(fileID).get()) {
                        try {
                            String ext = getFlag(fileID, "extension", "iufs").get();
                            String compression = getFlag(fileID, "compression", "none").get();
                            File tempFile = new File(ioservice.getTempFile(fileID, ext, compression).get());
                            FileContainer tempContainer = new FileContainer(tempFile, fileID);
                            log.info("Created temp file for relocation: {}", tempFile.getAbsolutePath());

                            UniversalSignable<FileContainer> signedFilePath = new UniversalSignable<FileContainer>(
                                    tempContainer);
                            // Set this user as original owner of the file
                            signedFilePath.setSignatory(cryptoservice.getPublicKey().get());
                            cryptoservice.sign().accept(signedFilePath);

                            new Thread(() -> {
                                try {
                                    ioservice.approvePath(tempContainer).get();

                                    service.save(ownServiceID, signedFilePath, ext)
                                            .addResultListener(new ResultObjectListener<FileID>(nodeCalls) {
                                                @Override
                                                public void resultAvailable(FileID result) {
                                                    rtn.addIntermediateResult(result);
                                                    isFinished();
                                                }

                                                @Override
                                                public void exceptionOccurred(Exception exception) {
                                                    rtn.setException(exception);
                                                    isFinished();
                                                    exception.printStackTrace();
                                                }

                                                private void isFinished() {
                                                    if (objects[0] instanceof IntRefHolder) {
                                                        IntRefHolder ref = (IntRefHolder) objects[0];
                                                        ref.value++;

                                                        if (ref.value >= relocationMap.size()) {
                                                            tempFile.delete();
                                                            log.info("All relocations completed");
                                                            rtn.setFinished();
                                                        }
                                                    }
                                                }
                                            });
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    rtn.setException(e);
                                }
                            }).start();

                        } catch (IOException | CryptoException e1) {
                            e1.printStackTrace();
                            rtn.setException(e1);
                        }
                    }
                }

                return Future.DONE;
            }
        });

        return rtn;
    }

    @Override
    public IFuture<String> getUUID() {
        return new Future<>(myUUID);
    }

    @Override
    public IFuture<IServiceIdentifier> getServiceID() {
        return new Future<IServiceIdentifier>(ownServiceID);
    }

    @Override
    public IFuture<PubKey> getPubKey() throws CryptoException {
        return cryptoservice.getPublicKey();
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<FileID> save(IServiceIdentifier serviceID, UniversalSignable<FileContainer> file, String ext) {
        if (!isResponsible(myUUID, file.getObject().getID()).get()) {
            return new Future<FileID>(new IOException("This node is not responsible for this file."));
        }
        return new Future<FileID>(ioservice.save(serviceID, file, ext).get());
    }

    @Override
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getStream(FileID id, int bufferSize) throws IOException {
        return ioservice.getStream(id, bufferSize);
    }

    @Override
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getFile(FileContainer file, int bufferSize) throws IOException {
        return ioservice.getFile(file, bufferSize);
    }

    @Override
    public IFuture<Boolean> fileExists(FileID id) {
        return ioservice.fileExists(id);
    }

    @Override
    public IFuture<Boolean> delete(FileID id) {
        Future<Boolean> rtn = new Future<Boolean>();

        if (isResponsible(myUUID, id).get()) {
            UniversalSignable<FileID> signedID = new UniversalSignable<FileID>(id);

            try {
                signedID.setSignatory(cryptoservice.getPublicKey().get());
                cryptoservice.sign().accept(signedID);

                return ioservice.deleteSecured(signedID, ownServiceID);
            } catch (CryptoException e) {
                rtn.setException(e);
            }
        } else {
            rtn.setException(new IOException("This node is not responsible for this file."));
        }

        return rtn;
    }

    @Override
    public IFuture<String> getFlag(FileID id, String key, String defaultValue) {
        return filedbservice.getFlag(id, key, defaultValue);
    }

    @Override
    public IFuture<Boolean> setFlag(FileID id, FileFlag flags) {
        return setFlags(id, new UniversalSignable<FileFlag[]>(new FileFlag[] { flags }));
    }

    @Override
    public IFuture<Boolean> setFlags(FileID id, FileFlag[] flags) {
        return setFlags(id, new UniversalSignable<FileFlag[]>(flags));
    }

    private IFuture<Boolean> setFlags(FileID id, UniversalSignable<FileFlag[]> signedFlags) {
        Future<Boolean> rtn = new Future<Boolean>();

        if (isResponsible(myUUID, id).get()) {
            try {
                signedFlags.setSignatory(cryptoservice.getPublicKey().get());
                cryptoservice.sign().accept(signedFlags);
                return filedbservice.setFlagsSecured(id, signedFlags, ownServiceID);
            } catch (CryptoException e) {
                rtn.setException(e);
            }
        } else {
            rtn.setException(new IOException("This node is not responsible for this file."));
        }

        return rtn;
    }

    @Override
    public void finalize() {
        onAgentKilled();
    }

    @Override
    public IFuture<Boolean> isFullNode() {
        return propertiesservice.getBool("fullNode");
    }
}
