package eu.roebert.iufs.neighbours;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.cadeia.blockchain.crypto.CryptoException;

import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.PubKey;
import eu.roebert.iufs.model.RendezvousList;
import eu.roebert.iufs.model.UniversalSignable;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;
import jadex.commons.future.ISubscriptionIntermediateFuture;

@Service
public interface INeighbourhoodService {
    /**
     * Returns an arbitrary node that is responsible for storing the file with the
     * given ID
     * 
     * @param fileID ID of the file
     * @return responsible node or null iff no such node could be found
     */
    public IFuture<IServiceIdentifier> getResponsibleNode(FileID fileID);

    /**
     * Returns a map of all uuids and nodes that are responsible for storing the
     * file with the given ID, sorted deterministically by degree of responsibility
     * in descending order.
     * 
     * @param fileID ID of the file
     * @return list of responsible nodes with their uuids
     */
    public IFuture<RendezvousList> getResponsibleNodes(FileID fileID);

    /**
     * Returns a list of all node uuids that are responsible for storing the file
     * with the given ID, sorted deterministically by degree of responsibility in
     * descending order.
     * 
     * @param fileID ID of the file
     * @return list of responsible nodes
     */
    public IFuture<List<String>> getResponsibleNodesUUIDs(FileID fileID);

    /**
     * Returns a list of all node ServiceIdentifiers that are responsible for
     * storing the file with the given ID, sorted deterministically by degree of
     * responsibility in descending order.
     * 
     * @param fileID ID of the file
     * @return list of responsible nodes
     */
    public IFuture<List<IServiceIdentifier>> getResponsibleNodesSIDs(FileID fileID);

    /**
     * Returns true iff the given node is responsible for storing the file with the
     * given ID
     * 
     * @param uuid   the node uuid
     * @param fileID ID of the file
     * @return true iff the node is responsible
     */
    public IFuture<Boolean> isResponsible(String uuid, FileID fileID);

    /**
     * Relocates all files that are already on the system to the responsible nodes.
     * If this node is not responsible for them anymore, they will be deleted.
     */
    public IFuture<Void> propagateEarlyFiles();

    /**
     * Called ONLY remotely to notify other nodes that this node is now part of the
     * network. Inserts the given node into the neighbourhood. Checks if there are
     * any local files that this node is not responsible for anymore, uploads them
     * to the new node if that node is responsible for them and deletes them
     * locally.
     * 
     * @param uuid         the new neighbour's uuid
     * @param newNeighbour the new neighbour's sid
     * @return true iff the given uuid isn't the local one AND the uuid wasn't
     *         already a known neighbour
     */
    public IFuture<Boolean> addNeighbour(String uuid, IServiceIdentifier sid);

    /**
     * Called locally if an absent node has been detected and remotely if a node
     * notifies the other nodes of its absence. Removes the given node from the
     * neighbourhood. Checks if there are any local files that the removed node was
     * responsible for. If this node is now on responsibility rank 1 for this file,
     * the file will be copied to the newly responsible node.
     * 
     * @param uuid the neighbour's uuid to delete
     */
    public IFuture<Void> removeNeighbour(String uuid);

    /**
     * Uploads a list of files to its specific node in the IUFS network.
     * 
     * @param relocationMap map of files and the nodes they should be uploaded to
     * @return the file IDs or null on error
     */
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<FileID> relocate(Map<FileID, IServiceIdentifier> relocationMap);

    /**
     * @return Returns the uuid of this node.
     */
    public IFuture<String> getUUID();

    /**
     * @return Returns the IServiceIdentifier of this service.
     */
    public IFuture<IServiceIdentifier> getServiceID();

    /**
     * @return Returns the public key as PubKey object.
     * @throws CryptoException on failure
     */
    public IFuture<PubKey> getPubKey() throws CryptoException;

    /**
     * Saves a file with given extension.
     * 
     * @param serviceID the id of the service
     * @param file      signed FileContainer of the file
     * @param ext       file extension
     * @return unique file identification
     * @throws IOException if an I/O error occurs
     */
    @Timeout(Timeout.NONE)
    public IFuture<FileID> save(IServiceIdentifier serviceID, UniversalSignable<FileContainer> file, String ext)
            throws IOException;

    /**
     * Returns the file under the given unique file identification.
     * 
     * @param id         Unique file identification
     * @param bufferSize the size of the buffer that the receiver expect
     * @return fileStream as SubscriptionIntermediateFuture byte array stream
     * @throws IOException If an I/O error occurs
     */
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getStream(FileID id, int bufferSize) throws IOException;

    /**
     * Returns the file under the given path, iff this path approved for upload on
     * the requested node.
     * 
     * <i>This method is typically internal used in IUFS.</i>
     * 
     * @param file       FileContainer of the file
     * @param bufferSize the size of the buffer that the receiver expect
     * @return the file as an subscription
     * @throws IOException if an I/O error occurs
     */
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<byte[]> getFile(FileContainer file, int bufferSize) throws IOException;

    /**
     * @param id Unique file identification
     * @return Checks if a file exists on the file system
     */
    public IFuture<Boolean> fileExists(FileID id);

    /**
     * Deletes a file under the given unique file identification from the file
     * system.
     * 
     * @param id Unique file identification
     * @return true iff the key existed and the deletion was successful
     */
    public IFuture<Boolean> delete(FileID id);

    /**
     * Returns a flag of the requested file.
     * 
     * @param key          the key for the flag
     * @param id           Unique file identification
     * @param defaultValue the default value that should be set if nothing was found
     * @return Returns a file flag
     */
    public IFuture<String> getFlag(FileID id, String key, String defaultValue);

    /**
     * Adds a flag to the file under the given unique file identification.
     * 
     * @param id   Unique file identification
     * @param flag File flag
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlag(FileID id, FileFlag flag);

    /**
     * Adds flags to the file under the given unique file identification.
     * 
     * @param id    Unique file identification
     * @param flags List of file flags
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlags(FileID id, FileFlag[] flags);

    /**
     * @return if this node also saves and serves files.
     */
    public IFuture<Boolean> isFullNode();
}
