//package eu.roebert.iufs.neighbours;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.HashMap;
//import java.util.List;
//import java.util.Map;
//import java.util.Random;
//import java.util.Set;
//
//import org.cadeia.blockchain.crypto.CryptoException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import eu.roebert.iufs.io.IOService;
//import eu.roebert.iufs.model.FileContainer;
//import eu.roebert.iufs.model.FileFlag;
//import eu.roebert.iufs.model.FileID;
//import eu.roebert.iufs.model.PubKey;
//import eu.roebert.iufs.model.UniversalSignable;
//import eu.roebert.iufs.properties.IPropertiesService;
//import eu.roebert.iufs.relocation.event.IRelocationEventService;
//import eu.roebert.iufs.security.ICryptoService;
//import jadex.bridge.IInternalAccess;
//import jadex.bridge.component.IArgumentsResultsFeature;
//import jadex.bridge.component.IExecutionFeature;
//import jadex.bridge.service.IService;
//import jadex.bridge.service.IServiceIdentifier;
//import jadex.bridge.service.annotation.Service;
//import jadex.bridge.service.annotation.ServiceComponent;
//import jadex.bridge.service.annotation.Timeout;
//import jadex.bridge.service.component.IRequiredServicesFeature;
//import jadex.bridge.service.search.SServiceProvider;
//import jadex.commons.future.Future;
//import jadex.commons.future.IFuture;
//import jadex.commons.future.ISubscriptionIntermediateFuture;
//import jadex.micro.annotation.Agent;
//import jadex.micro.annotation.AgentBody;
//import jadex.micro.annotation.AgentFeature;
//import jadex.micro.annotation.AgentService;
//import jadex.micro.annotation.Binding;
//import jadex.micro.annotation.ProvidedService;
//import jadex.micro.annotation.ProvidedServices;
//import jadex.micro.annotation.RequiredService;
//import jadex.micro.annotation.RequiredServices;
//
///**
// * In this DHT implementation is every node for every file responsible.
// */
//@Agent
//@Service
//@RequiredServices({
//        @RequiredService(name = "neighbourhoodservices", type = INeighbourhoodService.class, multiple = true, binding = @Binding(scope = Binding.SCOPE_GLOBAL)),
//        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "ioservice", type = IOService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "cryptoservice", type = ICryptoService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "relocationEventService", type = IRelocationEventService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "self", type = INeighbourhoodService.class, binding = @Binding(scope = Binding.SCOPE_COMPONENT)) })
//@ProvidedServices({ @ProvidedService(type = INeighbourhoodService.class, scope = Binding.SCOPE_GLOBAL) })
//public class SimpleFloodingAgent implements INeighbourhoodService {
//    private static final Logger log = LoggerFactory.getLogger(SimpleFloodingAgent.class);
//
//    public static final String KEY_PEER_DISCOVERY_INITIAL_WAIT = "initial_wait";
//    public static final String KEY_PEER_DISCOVERY_PERIODIC_WAIT = "periodic_wait";
//    public static final int REDUNDANCY = 1;
//
//    @AgentService(lazy = false)
//    IPropertiesService propertiesservice;
//    
//    @Agent
//    IInternalAccess agent;
//
//    @AgentService(lazy = false)
//    IOService ioservice;
//
//    @AgentService(lazy = false)
//    ICryptoService cryptoservice;
//
//    @AgentService(lazy = false)
//    IRelocationEventService relocationEventService;
//
//    @AgentService(lazy = false)
//    INeighbourhoodService self;
//
//    @AgentFeature
//    IRequiredServicesFeature requiredServices;
//
//    @AgentFeature
//    IExecutionFeature exe;
//
//    @AgentFeature
//    IArgumentsResultsFeature arguments;
//
//    @ServiceComponent
//    private IInternalAccess agentAccess;
//
//    private HashMap<String, IServiceIdentifier> nodes;
//    private String myUUID;
//
//    private IServiceIdentifier ownServiceID;
//
//    public SimpleFloodingAgent() {
//    }
//
//    @AgentBody
//    public void onAgentBody() {
//        log.info("{} Agent Started", this);
//
//        ownServiceID = ((IService) self).getServiceIdentifier();
//        nodes = new HashMap<String, IServiceIdentifier>();
//        myUUID = propertiesservice.get("uuid").get();
//
//        Map<String, Object> args = arguments.getResults();
//        long initial = (long) args.getOrDefault(KEY_PEER_DISCOVERY_INITIAL_WAIT, 300L);
//        long timeout = (long) args.getOrDefault(KEY_PEER_DISCOVERY_PERIODIC_WAIT, 10000L);
//
//        exe.repeatStep(initial, timeout, ia -> {
//            updateNeighbourhood();
//            return IFuture.DONE;
//        });
//    }
//
//    private void updateNeighbourhood() {
//
////		for (Entry<String, IIUFSService> node : nodes.entrySet()) {
////			try {
////				node.getValue().ping().get(5000);
////			} catch (Exception e) {
////				log.debug("Lost connection to node {}", node.getKey());
////				nodes.remove(node.getKey());
////			}
////		}
//        Set<String> oldIDs = nodes.keySet();
//        nodes = new HashMap<String, IServiceIdentifier>();
//
//        requiredServices.searchServices(INeighbourhoodService.class, Binding.SCOPE_GLOBAL)
//                .addIntermediateResultListener(result -> {
//                    INeighbourhoodService neighbour = (INeighbourhoodService) result;
//                    IService service = (IService) neighbour;
//                    IServiceIdentifier sid = service.getServiceIdentifier();
//                    String neighbourID = neighbour.getID().get();
//
//                    nodes.put(neighbourID, sid);
//
//                    if (!neighbourID.equals(myUUID) && !oldIDs.contains(neighbourID)) {
//                        log.info("Found new node with uuid: {}", neighbour.getID().get());
//                    }
//                });
//    }
//
//    @Override
//    public IFuture<List<IServiceIdentifier>> getResponsibleNodes(String fileID) {
//        return new Future<>(new ArrayList<IServiceIdentifier>(nodes.values()));
//    }
//
//    @Override
//    public IFuture<IServiceIdentifier> getResponsibleNode(String fileID) {
//        Random rand = new Random();
//
//        List<IServiceIdentifier> respNodes = new ArrayList<IServiceIdentifier>(nodes.values());
//        IServiceIdentifier neighbour = null;
//
//        if (respNodes.size() > 0) {
//            neighbour = respNodes.get(rand.nextInt(respNodes.size()));
//        } else {
//            return new Future<>((IServiceIdentifier) null);
//        }
//
//        return new Future<>(neighbour);
//    }
//
//    @Override
//    public IFuture<Boolean> isResponsible(String uuid, String fileID) {
//        return propertiesservice.getBool("fullNode");
//    }
//
//    @Override
//    public IFuture<Void> addNeighbour(String uuid, IServiceIdentifier newNeighbour) {
//        nodes.put(uuid, newNeighbour);
//
//        return Future.DONE;
//    }
//
//    @Override
//    public IFuture<Void> removeNeighbour(String uuid) {
//        nodes.remove(uuid);
//
//        return Future.DONE;
//    }
//
//    @Override
//    public IFuture<Boolean> updateNewNode(String uuid, IServiceIdentifier sid) {
//        addNeighbour(uuid, sid);
//        
//        for(IServiceIdentifier node : nodes.values()) {
//        	INeighbourhoodService service = (INeighbourhoodService) SServiceProvider
//                    .getService(agent, node, true).get();
//        	service.addNeighbour(uuid, sid);
//        }
//
//        return Future.TRUE;
//    }
//
//    @Override
//    public IFuture<Boolean> updateAbsentNode(String uuid) {
//        nodes.remove(uuid);
//        
//        for(IServiceIdentifier sid : nodes.values()) {
//        	INeighbourhoodService service = (INeighbourhoodService) SServiceProvider
//                    .getService(agent, sid, true).get();
//        	service.removeNeighbour(uuid);
//        }
//
//        return Future.TRUE;
//    }
//
//    @Override
//    public IFuture<String> getID() {
//        return new Future<>(myUUID);
//    }
//
//    @Override
//    @Timeout(Timeout.NONE)
//    public IFuture<FileID> save(IServiceIdentifier serviceID, UniversalSignable<FileContainer> file, String ext)
//            throws IOException {
//        if (isResponsible(myUUID, file.getObject().getID().getId()).get()) {
//            ioservice.save(serviceID, file, ext).get();
//            return new Future<FileID>(file.getObject().getID());
//        } else {
//            throw new IOException("This node is not responsible for this file.");
//        }
//    }
//
//    @Override
//    @Timeout(Timeout.NONE)
//    public ISubscriptionIntermediateFuture<byte[]> getStream(FileID id, int bufferSize) throws IOException {
//        if (isResponsible(myUUID, id.getId()).get()) {
//            return ioservice.getStream(id, bufferSize);
//        } else {
//            throw new IOException("This node is not responsible for this file.");
//        }
//    }
//
//    @Override
//    @Timeout(Timeout.NONE)
//    public ISubscriptionIntermediateFuture<byte[]> getFile(FileContainer file, int bufferSize) throws IOException {
//        if (isResponsible(myUUID, file.getID().getId()).get()) {
//            return ioservice.getFile(file, bufferSize);
//        } else {
//            throw new IOException("This node is not responsible for this file.");
//        }
//
//    }
//
//    @Override
//    public IFuture<String> getFlag(FileID id, String key, String defaultValue) {
//        if (isResponsible(myUUID, id.getId()).get()) {
//            return ioservice.getFlag(id, key, defaultValue);
//        } else {
//            Future<String> rtn = new Future<String>();
//            rtn.setException(new IOException("This node is not responsible for this file."));
//
//            return rtn;
//        }
//    }
//
//    @Override
//    public IFuture<Boolean> fileExists(FileID id) {
//        if (isResponsible(myUUID, id.getId()).get()) {
//            return ioservice.fileExists(id);
//        } else {
//            Future<Boolean> rtn = new Future<Boolean>();
//            rtn.setException(new IOException("This node is not responsible for this file."));
//
//            return rtn;
//        }
//    }
//
//    @Override
//    public IFuture<PubKey> getPubKey() throws CryptoException {
//        return cryptoservice.getPublicKey();
//    }
//
//    @Override
//    public IFuture<Void> setFlag(FileID id, FileFlag flag) {
//        Future<Void> rtn = new Future<Void>();
//
//        if (isResponsible(myUUID, id.getId()).get()) {
//            UniversalSignable<FileFlag> signedFlag = new UniversalSignable<FileFlag>(flag);
//
//            try {
//                signedFlag.setSignatory(cryptoservice.getPublicKey().get());
//                cryptoservice.sign().accept(signedFlag);
//
//                return ioservice.setFlagSecured(id, signedFlag, ownServiceID);
//            } catch (CryptoException e) {
//                rtn.setException(e);
//            }
//        } else {
//            rtn.setException(new IOException("This node is not responsible for this file."));
//        }
//
//        return rtn;
//    }
//
//    @Override
//    public IFuture<Void> setFlags(FileID id, FileFlag[] flags) {
//        Future<Void> rtn = new Future<Void>();
//
//        if (isResponsible(myUUID, id.getId()).get()) {
//            UniversalSignable<FileFlag[]> signedFlags = new UniversalSignable<FileFlag[]>(flags);
//
//            try {
//                signedFlags.setSignatory(cryptoservice.getPublicKey().get());
//                cryptoservice.sign().accept(signedFlags);
//
//                return ioservice.setFlagsSecured(id, signedFlags, ownServiceID);
//            } catch (CryptoException e) {
//                rtn.setException(e);
//            }
//        } else {
//            rtn.setException(new IOException("This node is not responsible for this file."));
//        }
//
//        return rtn;
//    }
//
//    @Override
//    public IFuture<IServiceIdentifier> getServiceID() {
//        return new Future<IServiceIdentifier>(ownServiceID);
//    }
//
//    @Override
//    public IFuture<Void> delete(FileID id) {
//        Future<Void> rtn = new Future<Void>();
//
//        if (isResponsible(myUUID, id.getId()).get()) {
//            UniversalSignable<FileID> signedID = new UniversalSignable<FileID>(id);
//
//            try {
//                signedID.setSignatory(cryptoservice.getPublicKey().get());
//                cryptoservice.sign().accept(signedID);
//
//                return ioservice.deleteSecured(signedID, ownServiceID);
//            } catch (CryptoException e) {
//                rtn.setException(e);
//            }
//        } else {
//            rtn.setException(new IOException("This node is not responsible for this file."));
//        }
//
//        return rtn;
//    }
//}
