package eu.roebert.iufs.future;

import java.io.OutputStream;

import eu.roebert.iufs.model.FileID;
import jadex.commons.future.Future;
import jadex.commons.future.IIntermediateResultListener;

/**
 * Result listener with additional notifications in case of intermediate results
 * and Constructor for two OutputStreams and one FileID Future.
 */
public abstract class IntermediateStreamResultListener<E> implements IIntermediateResultListener<E> {
    protected OutputStream o1;
    protected OutputStream o2;
    protected Future<FileID> o3;

    public IntermediateStreamResultListener(OutputStream o1, OutputStream o2, Future<FileID> o3) {
        this.o1 = o1;
        this.o2 = o2;
        this.o3 = o3;
    }
}
