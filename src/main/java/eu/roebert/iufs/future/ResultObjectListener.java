package eu.roebert.iufs.future;

import jadex.commons.future.IResultListener;

public abstract class ResultObjectListener<E> implements IResultListener<E> {
    protected Object[] objects;

    public ResultObjectListener(Object... objects) {
        this.objects = objects;
    }
}
