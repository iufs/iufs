package eu.roebert.iufs.core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.roebert.iufs.exceptions.PropertiesException;
import eu.roebert.iufs.properties.PropertiesFileSystemAgent;
import jadex.base.PlatformConfiguration;
import jadex.base.Starter;
import jadex.bridge.IExternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.commons.Tuple2;
import jadex.commons.future.ISubscriptionIntermediateFuture;

public class IUFSStartup {
    public static boolean GUI;
    public static boolean JADEXGUI;
    public static boolean DEBUG;
    public static boolean MONITORING;
    public static String NETWORKNAME;
    public static String NETWORKPASSWORD;
    public static String UUID;
    private IStartupComponents starter;

    public IUFSStartup() {
        this(new ArrayList<String>());
    }

    public IUFSStartup(List<String> args) {
        PropertiesFileSystemAgent properties = new PropertiesFileSystemAgent();
        properties.arguments = new IArgumentsResultsFeature() {

            @Override
            public ISubscriptionIntermediateFuture<Tuple2<String, Object>> subscribeToResults() {
                return null;
            }

            @Override
            public Map<String, Object> getResults() {
                return null;
            }

            @Override
            public Map<String, Object> getArguments() {
                HashMap<String, Object> map = new HashMap<>();
                map.put("location", "configs");
                return map;
            }
        };

        try {
            properties.created();
            NETWORKNAME = properties.get("networkName").get();
            NETWORKPASSWORD = properties.get("networkPassword").get();
            UUID = properties.get("uuid").get();
        } catch (PropertiesException e) {
            e.printStackTrace();
        }

        PlatformConfiguration config = PlatformConfiguration.getDefaultNoGui();
        config.setLogging(args.contains("logging=true"));
        config.setDebugFutures(args.contains("debug=true"));
        config.setDebugServices(args.contains("debug=true"));
        config.setDebugSteps(args.contains("debug=true"));
        config.setGui(args.contains("gui=true"));

        config.setPlatformName("IUFS-Node_" + UUID);
        config.setNetworkName(NETWORKNAME);
        config.setNetworkPass(NETWORKPASSWORD);

        IExternalAccess platform = Starter.createPlatform(config).get();

        if (properties.getBool("fullNode").get()) {
            starter = new FullNodeStartupComponents(platform);
        } else {
            starter = new ThinNodeStartupComponents(platform);
        }
    }

    public static void main(String[] arg0) {
        List<String> args = Arrays.asList(arg0);
        new IUFSStartup(args);
    }

    public IStartupComponents getStarter() {
        return starter;
    }
}
