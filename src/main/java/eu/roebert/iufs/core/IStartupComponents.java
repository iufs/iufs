package eu.roebert.iufs.core;

public interface IStartupComponents {
    /**
     * Helper method to register an agent at the Jadex platform.
     * 
     * @param clazz  class that should be register
     * @param keys   keys for agent arguments
     * @param values values for agent arguments
     */
    @SuppressWarnings("rawtypes")
    public void createComponent(Class clazz, String[] keys, String[] values);

    /**
     * Helper method to register an agent at the Jadex platform, with empty
     * arguments.
     * 
     * @param clazz class that should be register
     */
    @SuppressWarnings("rawtypes")
    public void createComponent(Class clazz);
}
