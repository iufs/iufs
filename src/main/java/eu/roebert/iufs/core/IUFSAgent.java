package eu.roebert.iufs.core;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.cadeia.blockchain.crypto.CryptoException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.roebert.iufs.compression.CompressionFactory;
import eu.roebert.iufs.future.ResultObjectListener;
import eu.roebert.iufs.io.IOService;
import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.IntRefHolder;
import eu.roebert.iufs.model.Pair;
import eu.roebert.iufs.model.UniversalSignable;
import eu.roebert.iufs.neighbours.INeighbourhoodService;
import eu.roebert.iufs.properties.IPropertiesService;
import eu.roebert.iufs.security.ICryptoService;
import jadex.bridge.IInternalAccess;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.Future;
import jadex.commons.future.IFuture;
import jadex.commons.future.IIntermediateResultListener;
import jadex.commons.future.ISubscriptionIntermediateFuture;
import jadex.commons.future.SubscriptionIntermediateFuture;
import jadex.micro.annotation.Agent;
import jadex.micro.annotation.AgentBody;
import jadex.micro.annotation.AgentCreated;
import jadex.micro.annotation.AgentFeature;
import jadex.micro.annotation.AgentService;
import jadex.micro.annotation.Binding;
import jadex.micro.annotation.ProvidedService;
import jadex.micro.annotation.ProvidedServices;
import jadex.micro.annotation.RequiredService;
import jadex.micro.annotation.RequiredServices;

@Agent
@Service
@ProvidedServices({ @ProvidedService(name = "iufsservice", type = IIUFSService.class, scope = Binding.SCOPE_PLATFORM) })
@RequiredServices({
        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "neighbourhoodservice", type = INeighbourhoodService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "ioservice", type = IOService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
        @RequiredService(name = "cryptoservice", type = ICryptoService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
public class IUFSAgent implements IIUFSService {
    private static final Logger LOG = LoggerFactory.getLogger(IUFSAgent.class);
    protected HashMap<String, ISubscriptionIntermediateFuture<byte[]>> listeners;

    @AgentService(lazy = false)
    IPropertiesService propertiesservice;

    @AgentService(lazy = false)
    INeighbourhoodService neighbourhoodservice;

    @Agent
    IInternalAccess agent;

    @AgentService(lazy = false)
    IOService ioservice;

    @AgentService(lazy = false)
    ICryptoService cryptoservice;

    @AgentFeature
    IArgumentsResultsFeature arguments;

    public IUFSAgent() {
        listeners = new HashMap<>();
    }

    @AgentCreated
    public void created() {
        LOG.debug("Agent created");
    }

    @AgentBody
    public void onAgentBody() {
        LOG.info("{} Agent Started", this);
    }

    @Override
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<Pair<FileID, IServiceIdentifier>> upload(FileContainer filePath) {
        SubscriptionIntermediateFuture<Pair<FileID, IServiceIdentifier>> rtn = new SubscriptionIntermediateFuture<>();

        new Thread(() -> {
            try {
                UniversalSignable<FileContainer> signedFilePath = new UniversalSignable<FileContainer>(filePath);
                // Set this user as original owner of the file
                signedFilePath.setSignatory(cryptoservice.getPublicKey().get());
                IntRefHolder nodeCalls = new IntRefHolder(0);
                cryptoservice.sign().accept(signedFilePath);
                FileID fileID = filePath.getID();

                IServiceIdentifier ownCID = neighbourhoodservice.getServiceID().get();
                List<IServiceIdentifier> nodes = neighbourhoodservice.getResponsibleNodesSIDs(fileID).get();

                for (IServiceIdentifier neighbour : nodes) {
                    new Thread(() -> {
                        ioservice.approvePath(filePath).get();
                        INeighbourhoodService service = null;
                        while (true) {
                            try {
                                service = (INeighbourhoodService) SServiceProvider.getService(agent, neighbour, true)
                                        .get();
                                break;
                            } catch (RuntimeException e) {
                                continue;
                            }
                        }

                        try {
                            service.save(ownCID, signedFilePath, filePath.getExtension())
                                    .addResultListener(new ResultObjectListener<FileID>(nodeCalls) {
                                        @Override
                                        public void resultAvailable(FileID result) {
                                            rtn.addIntermediateResult(
                                                    new Pair<FileID, IServiceIdentifier>(result, neighbour));
                                            isFinished();
                                        }

                                        @Override
                                        public void exceptionOccurred(Exception exception) {
                                            rtn.setException(exception);
                                            isFinished();
                                            exception.printStackTrace();
                                        }

                                        private void isFinished() {
                                            if (objects[0] instanceof IntRefHolder) {
                                                IntRefHolder ref = (IntRefHolder) objects[0];
                                                ref.value++;

                                                if (ref.value >= nodes.size()) {
                                                    rtn.setFinished();
                                                }
                                            }
                                        }
                                    });
                        } catch (IOException e) {
                            e.printStackTrace();
                            rtn.setException(e);
                        }
                    }).start();
                }

            } catch (CryptoException e1) {
                e1.printStackTrace();
                rtn.setException(e1);
            }
        }).start();

        return rtn;
    }

    @Override
    @Timeout(Timeout.NONE)
    public IFuture<FileContainer> download(FileID fileID) {
        Future<FileContainer> rtn = new Future<>();

        new Thread(() -> {
            IServiceIdentifier node = neighbourhoodservice.getResponsibleNode(fileID).get();

            if (node != null) {
                INeighbourhoodService service = null;
                while (true) {
                    try {
                        service = (INeighbourhoodService) SServiceProvider.getService(agent, node, true).get();
                        break;
                    } catch (RuntimeException e) {
                        continue;
                    }
                }

                if (!service.fileExists(fileID).get()) {
                    for (IServiceIdentifier newNode : neighbourhoodservice.getResponsibleNodesSIDs(fileID).get()) {
                        service = (INeighbourhoodService) SServiceProvider.getService(agent, newNode, true).get();
                        if (service.fileExists(fileID).get()) {
                            break;
                        }
                    }
                }

                if (service.fileExists(fileID).get()) {
                    String ext = service.getFlag(fileID, "extension", "iufs").get();
                    String compression = service.getFlag(fileID, "compression", "none").get();

                    File tempFile;
                    File decompressedTempFile;
                    try {
                        tempFile = File.createTempFile(fileID.getId(), "." + ext);
                        tempFile.deleteOnExit();
                        decompressedTempFile = File.createTempFile(fileID.getId(), "-decompressed." + ext);
                        decompressedTempFile.deleteOnExit();
                        FileOutputStream fout = new FileOutputStream(tempFile);
                        OutputStream out = new BufferedOutputStream(fout, 1024 * 1024);

                        try {
                            ISubscriptionIntermediateFuture<byte[]> fileChunks = service.getStream(fileID,
                                    propertiesservice.getInt("bufferSize").get());

                            String listenerID = UUID.randomUUID().toString();
                            listeners.put(listenerID, fileChunks);

                            fileChunks.addResultListener(new IIntermediateResultListener<byte[]>() {
                                @Override
                                public void resultAvailable(Collection<byte[]> result) {
                                }

                                @Override
                                public void exceptionOccurred(Exception exception) {
                                    LOG.error(exception.getMessage());
                                    exception.printStackTrace();
                                    listeners.remove(listenerID);
                                    try {
                                        fout.close();
                                        out.close();
                                        tempFile.delete();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                    rtn.setException(exception);
                                }

                                @Override
                                public void intermediateResultAvailable(byte[] result) {
                                    try {
                                        out.write(result, 0, result.length);
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        rtn.setException(new IOException(e.getMessage()));
                                    }
                                }

                                @Override
                                public void finished() {
                                    try {
                                        out.close();

                                        CompressionFactory.get(compression).decompress(tempFile, decompressedTempFile);
                                        tempFile.delete();
                                        rtn.setResult(new FileContainer(decompressedTempFile));
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                        rtn.setException(e);
                                    }
                                    listeners.remove(listenerID);
                                }
                            });
                        } catch (IOException e1) {
                            e1.printStackTrace();
                            rtn.setException(e1);
                        }
                    } catch (Exception e) {
                        rtn.setException(e);
                    }
                } else {
                    rtn.setException(new Exception("Can't find a responsible node that have the file."));
                }

            } else {
                rtn.setException(new NullPointerException("Responsible node is null."));
            }
        }).start();

        return rtn;
    }

    @Override
    public IFuture<String> getFlag(FileID fileID, String key, String defaultValue) {
        Future<String> rtn = new Future<>();

        new Thread(() -> {
            IServiceIdentifier node = neighbourhoodservice.getResponsibleNode(fileID).get();

            if (node != null) {
                INeighbourhoodService service = null;
                while (true) {
                    try {
                        service = (INeighbourhoodService) SServiceProvider.getService(agent, node, true).get();
                        break;
                    } catch (RuntimeException e) {
                        continue;
                    }
                }

                rtn.setResult(service.getFlag(fileID, key, defaultValue).get());
            } else {
                rtn.setResult(defaultValue);
            }
        }).start();

        return rtn;
    }

    @Override
    public IFuture<Boolean> delete(FileID id) {
        Future<Boolean> rtn = new Future<Boolean>();

        new Thread(() -> {
            List<IServiceIdentifier> nodes = neighbourhoodservice.getResponsibleNodesSIDs(id).get();

            for (IServiceIdentifier node : nodes) {
                new Thread(() -> {
                    INeighbourhoodService service = null;
                    while (true) {
                        try {
                            service = (INeighbourhoodService) SServiceProvider.getService(agent, node, true).get();
                            break;
                        } catch (RuntimeException e) {
                            continue;
                        }
                    }

                    rtn.setResult(service.delete(id).get());
                }).start();
            }
        }).start();

        return rtn;
    }

    @Override
    public IFuture<Boolean> setFlag(FileID id, FileFlag flag) {
        Future<Boolean> rtn = new Future<>();

        new Thread(() -> {
            List<IServiceIdentifier> nodes = neighbourhoodservice.getResponsibleNodesSIDs(id).get();

            for (IServiceIdentifier node : nodes) {
                new Thread(() -> {
                    INeighbourhoodService service = null;
                    while (true) {
                        try {
                            service = (INeighbourhoodService) SServiceProvider.getService(agent, node, true).get();
                            break;
                        } catch (RuntimeException e) {
                            continue;
                        }
                    }

                    rtn.setResult(service.setFlag(id, flag).get());
                }).start();
            }
        }).start();

        return rtn;
    }

    @Override
    public IFuture<Boolean> setFlags(FileID id, FileFlag[] flags) {
        Future<Boolean> rtn = new Future<Boolean>();

        new Thread(() -> {
            List<IServiceIdentifier> nodes = neighbourhoodservice.getResponsibleNodesSIDs(id).get();

            for (IServiceIdentifier node : nodes) {
                new Thread(() -> {
                    INeighbourhoodService service = null;
                    while (true) {
                        try {
                            service = (INeighbourhoodService) SServiceProvider.getService(agent, node, true).get();
                            break;
                        } catch (RuntimeException e) {
                            continue;
                        }
                    }

                    rtn.setResult(service.setFlags(id, flags).get());
                }).start();
            }
        }).start();

        return rtn;
    }
}
