package eu.roebert.iufs.core;

import eu.roebert.iufs.model.FileContainer;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import eu.roebert.iufs.model.Pair;
import jadex.bridge.service.IServiceIdentifier;
import jadex.bridge.service.annotation.Service;
import jadex.bridge.service.annotation.Timeout;
import jadex.commons.future.IFuture;
import jadex.commons.future.ISubscriptionIntermediateFuture;

/**
 * This service models the user view of the IUFS network. Simple functions for
 * up- and download.
 * 
 * Note: The methods of this service haves no timeout. So it is not recommended
 * to use a get call on the returning feature.
 */
@Service
public interface IIUFSService {
    /**
     * Uploads a file to the IUFS network. The nodes to upload to are determined by
     * the neighbourhood service.
     * 
     * @param filePath absolute path to file that should be uploaded
     * @return FileID or null on error
     */
    @Timeout(Timeout.NONE)
    public ISubscriptionIntermediateFuture<Pair<FileID, IServiceIdentifier>> upload(FileContainer filePath);

    /**
     * Downloads a file from the IUFS network.
     * 
     * @param fileID unique id of the file, that should be downloaded
     * @return File in temp folder or null on fail
     */
    @Timeout(Timeout.NONE)
    public IFuture<FileContainer> download(FileID fileID);

    /**
     * Deletes a file under the given unique file identification from the file
     * system.
     * 
     * @param id Unique file identification
     * @return true iff the key existed and the deletion was successful
     */
    public IFuture<Boolean> delete(FileID id);

    /**
     * Returns a flag of the requested file.
     * 
     * @param fileID       Unique file identification
     * @param key          the flag key
     * @param defaultValue the default value that should be set if nothing was found
     * @return Returns a file flags
     */
    public IFuture<String> getFlag(FileID fileID, String key, String defaultValue);

    /**
     * Adds a flag to the file under the given unique file identification.
     * 
     * @apiNote This method can only invoked by the original file owner and can also
     *          not be forwarded by a third party e.g. over P2P.
     * 
     * @param id   Unique file identification
     * @param flag File flag
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlag(FileID id, FileFlag flag);

    /**
     * Adds flags to the file under the given unique file identification.
     * 
     * @apiNote This method can only invoked by the original file owner and can also
     *          not be forwarded by a third party e.g. over P2P.
     * 
     * @param id    Unique file identification
     * @param flags List of file flags
     * @return true iff the key exists in the DB and the flag has been updated
     */
    public IFuture<Boolean> setFlags(FileID id, FileFlag[] flags);
}
