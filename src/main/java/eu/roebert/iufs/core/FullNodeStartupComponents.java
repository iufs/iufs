package eu.roebert.iufs.core;

import eu.roebert.iufs.avscan.AVDatabaseAgent;
import eu.roebert.iufs.avscan.AVMiddlewareAgent;
import eu.roebert.iufs.avscan.ClamAVAgent;
import eu.roebert.iufs.cron.QuartzCronAgent;
import eu.roebert.iufs.cron.starter.PermanentBackgroundTasksAgent;
import eu.roebert.iufs.filedb.FileDBAgent;
import eu.roebert.iufs.io.IOFileSystemAgent;
import eu.roebert.iufs.neighbours.RendezvousHashingAgent;
import eu.roebert.iufs.properties.PropertiesFileSystemAgent;
import eu.roebert.iufs.security.CryptoAgent;
import jadex.bridge.IExternalAccess;
import jadex.bridge.service.search.SServiceProvider;
import jadex.bridge.service.types.cms.CreationInfo;
import jadex.bridge.service.types.cms.IComponentManagementService;
import jadex.commons.SUtil;

public class FullNodeStartupComponents implements IStartupComponents {
    private IExternalAccess platform;
    IComponentManagementService cms;

    public FullNodeStartupComponents(IExternalAccess platform) {
        this.platform = platform;
        addComponents();
    }

    /**
     * Register all Jadex Agents here, there are supposed to be started.
     */
    private void addComponents() {
        cms = SServiceProvider.getService(platform, IComponentManagementService.class).get();
        createComponent(PropertiesFileSystemAgent.class, new String[] { "location" }, new String[] { "configs" });
        createComponent(CryptoAgent.class);
        createComponent(QuartzCronAgent.class);
        createComponent(AVDatabaseAgent.class);
        createComponent(ClamAVAgent.class);
        createComponent(AVMiddlewareAgent.class);
        createComponent(FileDBAgent.class);
        createComponent(IOFileSystemAgent.class);
        createComponent(RendezvousHashingAgent.class);
        createComponent(IUFSAgent.class);
        createComponent(PermanentBackgroundTasksAgent.class);
    }

    @SuppressWarnings("rawtypes")
    public void createComponent(Class clazz, String[] keys, String[] values) {
        cms.createComponent(clazz.getName() + ".class", new CreationInfo(SUtil.createHashMap(keys, values)))
                .getFirstResult();
    }

    @SuppressWarnings("rawtypes")
    public void createComponent(Class clazz) {
        createComponent(clazz, new String[] {}, new String[] {});
    }
}
