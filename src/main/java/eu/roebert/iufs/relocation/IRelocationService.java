//package eu.roebert.iufs.relocation;
//
//import java.util.Map;
//
//import eu.roebert.iufs.model.FileID;
//import jadex.bridge.service.IServiceIdentifier;
//import jadex.bridge.service.annotation.Service;
//import jadex.bridge.service.annotation.Timeout;
//import jadex.commons.future.ISubscriptionIntermediateFuture;
//
//@Service
//public interface IRelocationService {
//    /**
//     * You can call this method to manually start a relocate. Uploads a list of 
//     * files to its specific node in the IUFS network.
//     * 
//     * @param relocationMap map of files and the nodes they should be uploaded to
//     * @return FileID or null on error
//     */
//    @Timeout(Timeout.NONE)
//    public ISubscriptionIntermediateFuture<FileID> relocate(Map<FileID, IServiceIdentifier> relocationMap);
//}
