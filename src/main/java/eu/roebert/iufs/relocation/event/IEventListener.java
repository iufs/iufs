//package eu.roebert.iufs.relocation.event;
//
//import jadex.bridge.service.annotation.Reference;
//
///**
// * Listener for events.
// */
//@Reference
//public interface IEventListener<T> {
//    /**
//     * This method is called, when an event is fired.
//     * 
//     * @param eventData data of the event
//     */
//    public void eventPerformed(T eventData);
//}
