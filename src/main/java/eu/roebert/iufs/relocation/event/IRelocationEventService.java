//package eu.roebert.iufs.relocation.event;
//
//import java.util.Map;
//
//import eu.roebert.iufs.model.Event;
//import eu.roebert.iufs.model.FileID;
//import jadex.bridge.service.IServiceIdentifier;
//import jadex.bridge.service.annotation.Service;
//import jadex.commons.future.IFuture;
//import jadex.commons.future.ISubscriptionIntermediateFuture;
//
///**
// * The {@link IRelocationEventService} are informed by the
// * {@link INeighbourhoodService} when the neighbourhood changed and the
// * {@link IRelocationService} has to send files to the new neighbourhood or
// * peers. Therefore the {@link INeighbourhoodService} emit an event with a list
// * of the new neighbourhood.
// */
//@Service
//public interface IRelocationEventService {
//    /**
//     * Registers the {@code IEventListener}.
//     * 
//     * @param listener Listener that should be notified by events
//     * @return a future that is already done
//     */
//    public IFuture<Void> addListener(IEventListener<Map<FileID, IServiceIdentifier>> listener);
//
//    /**
//     * Removes the {@code IEventListener}.
//     * 
//     * @param listener Listener that should be removed
//     * @return a future that is already done
//     */
//    public IFuture<Void> removeListener(IEventListener<Map<FileID, IServiceIdentifier>> listener);
//
//    /**
//     * Emit an event.
//     * 
//     * @param event event that should be emitted
//     * @return an intermediate future returning the fileID of the relocated file
//     */
//    public ISubscriptionIntermediateFuture<FileID> emit(Event<Map<FileID, IServiceIdentifier>> event);
//}
