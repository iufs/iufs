//package eu.roebert.iufs.relocation.event;
//
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import eu.roebert.iufs.model.Event;
//import eu.roebert.iufs.model.FileID;
//import jadex.bridge.service.IServiceIdentifier;
//import jadex.bridge.service.annotation.Service;
//import jadex.commons.future.Future;
//import jadex.commons.future.IFuture;
//import jadex.commons.future.ISubscriptionIntermediateFuture;
//import jadex.micro.annotation.Agent;
//import jadex.micro.annotation.AgentCreated;
//import jadex.micro.annotation.Binding;
//import jadex.micro.annotation.ProvidedService;
//import jadex.micro.annotation.ProvidedServices;
//
//@Agent
//@Service
//@ProvidedServices({
//        @ProvidedService(name = "relocationEventService", type = IRelocationEventService.class, scope = Binding.SCOPE_PLATFORM) })
//public class RelocationEventAgent implements IRelocationEventService {
//    private static final Logger LOG = LoggerFactory.getLogger(RelocationEventAgent.class);
//    private List<IEventListener<Map<FileID, IServiceIdentifier>>> listeners = new LinkedList<>();
//
//    /**
//     * Empty constructor for Jadex. <b>Never use it by yourself.</b>
//     */
//    public RelocationEventAgent() {
//    }
//
//    @AgentCreated
//    public void created() {
//        LOG.info("Agent {} created.", this);
//    }
//
//    @Override
//    public IFuture<Void> addListener(IEventListener<Map<FileID, IServiceIdentifier>> listener) {
//        listeners.add(listener);
//
//        return Future.DONE;
//    }
//
//    @Override
//    public IFuture<Void> removeListener(IEventListener<Map<FileID, IServiceIdentifier>> listener) {
//        listeners.remove(listener);
//
//        return Future.DONE;
//    }
//
//    @Override
//    public ISubscriptionIntermediateFuture<FileID> emit(Event<Map<FileID, IServiceIdentifier>> event) {
//        new Thread(() -> {
//            for (IEventListener<Map<FileID, IServiceIdentifier>> listener : listeners) {
//                new Thread(() -> {
//                    listener.eventPerformed(event.getData());
//                }).start();
//            }
//        }).start();
//
//        return Future.DONE;
//    }
//}
