//package eu.roebert.iufs.relocation;
//
//import java.io.File;
//import java.io.IOException;
//import java.util.Map;
//import java.util.Map.Entry;
//
//import org.cadeia.blockchain.crypto.CryptoException;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import eu.roebert.iufs.future.ResultObjectListener;
//import eu.roebert.iufs.io.IOService;
//import eu.roebert.iufs.model.FileContainer;
//import eu.roebert.iufs.model.FileID;
//import eu.roebert.iufs.model.IntRefHolder;
//import eu.roebert.iufs.model.UniversalSignable;
//import eu.roebert.iufs.neighbours.INeighbourhoodService;
//import eu.roebert.iufs.properties.IPropertiesService;
//import eu.roebert.iufs.relocation.event.IEventListener;
//import eu.roebert.iufs.relocation.event.IRelocationEventService;
//import eu.roebert.iufs.security.ICryptoService;
//import jadex.bridge.IInternalAccess;
//import jadex.bridge.service.IServiceIdentifier;
//import jadex.bridge.service.annotation.Service;
//import jadex.bridge.service.annotation.Timeout;
//import jadex.bridge.service.search.SServiceProvider;
//import jadex.commons.future.ISubscriptionIntermediateFuture;
//import jadex.commons.future.SubscriptionIntermediateFuture;
//import jadex.micro.annotation.Agent;
//import jadex.micro.annotation.AgentCreated;
//import jadex.micro.annotation.AgentKilled;
//import jadex.micro.annotation.AgentService;
//import jadex.micro.annotation.Binding;
//import jadex.micro.annotation.ProvidedService;
//import jadex.micro.annotation.ProvidedServices;
//import jadex.micro.annotation.RequiredService;
//import jadex.micro.annotation.RequiredServices;
//
//@Agent
//@Service
//@ProvidedServices({
//        @ProvidedService(name = "relocationService", type = IRelocationService.class, scope = Binding.SCOPE_PLATFORM) })
//@RequiredServices({
//        @RequiredService(name = "relocationEventService", type = IRelocationEventService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "propertiesservice", type = IPropertiesService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "neighbourhoodservice", type = INeighbourhoodService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "ioservice", type = IOService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)),
//        @RequiredService(name = "cryptoservice", type = ICryptoService.class, binding = @Binding(scope = Binding.SCOPE_PLATFORM)) })
//public class RelocationAgent implements IRelocationService {
//    private static final Logger LOG = LoggerFactory.getLogger(RelocationAgent.class);
//    private IEventListener<Map<FileID, IServiceIdentifier>> listener;
//
//    @AgentService(lazy = false)
//    IRelocationEventService relocationEventService;
//
//    @AgentService(lazy = false)
//    IPropertiesService propertiesservice;
//
//    @AgentService(lazy = false)
//    INeighbourhoodService neighbourhoodservice;
//
//    @Agent
//    IInternalAccess agent;
//    
//    @AgentService(lazy = false)
//    IOService ioservice;
//
//    @AgentService(lazy = false)
//    ICryptoService cryptoservice;
//
//    @AgentCreated
//    public void created() {
//        registerListener();
//        LOG.info("Agent {} created.", this);
//    }
//
//    @AgentKilled
//    public void killed() {
//        relocationEventService.removeListener(listener).get();
//    }
//
//    private void registerListener() {
//        listener = new IEventListener<Map<FileID, IServiceIdentifier>>() {
//            @Override
//            public ISubscriptionIntermediateFuture<FileID> eventPerformed(Map<FileID, IServiceIdentifier> eventData) {
//                return relocate(eventData);
//            }
//        };
//
//        relocationEventService.addListener(listener).get();
//    }
//    
//    @Override
//    @Timeout(Timeout.NONE)
//    public ISubscriptionIntermediateFuture<FileID> relocate(Map<FileID, IServiceIdentifier> relocationMap) {
//    	SubscriptionIntermediateFuture<FileID> rtn = new SubscriptionIntermediateFuture<>();
//
//        new Thread(() -> {
//	        IntRefHolder nodeCalls = new IntRefHolder(0);
//	        
//	        for(Entry<FileID, IServiceIdentifier> entry : relocationMap.entrySet()) {
//            	FileID fileID = entry.getKey();
//            	IServiceIdentifier node = entry.getValue();
//            	
//            	if(!node.equals(neighbourhoodservice.getServiceID().get())) {
//		            try {
//			            String ext = neighbourhoodservice.getFlag(fileID, "extension", "iufs").get();
//			            String compression = neighbourhoodservice.getFlag(fileID, "compression", "none").get();
//			            System.err.println("getting temp file " + fileID + " " + ext + " " + compression);
//	                	File tempFile = new File(ioservice.getTempFile(fileID, ext, compression).get());
//			            System.err.println("gotten temp file");
//	                	FileContainer tempContainer = new FileContainer(tempFile, fileID);
//	                    LOG.info("Created temp file for relocation: {}", tempFile.getAbsolutePath());
//			            
//			            UniversalSignable<FileContainer> signedFilePath = new UniversalSignable<FileContainer>(tempContainer);
//			            // Set this user as original owner of the file
//			            signedFilePath.setSignatory(cryptoservice.getPublicKey().get());
//			            cryptoservice.sign().accept(signedFilePath);
//			            
//		                new Thread(() -> {
//			                try {
//				                ioservice.approvePath(tempContainer).get();
//				                INeighbourhoodService service = (INeighbourhoodService) SServiceProvider
//				                        .getService(agent, node, true).get();
//			
//			                    service.save(neighbourhoodservice.getServiceID().get(), signedFilePath, ext)
//			                            .addResultListener(new ResultObjectListener<FileID>(nodeCalls) {
//			                                @Override
//			                                public void resultAvailable(FileID result) {
//			                                    rtn.addIntermediateResult(result);
//			                                    isFinished();
//			                                }
//			                                
//			                                @Override
//			                                public void exceptionOccurred(Exception exception) {
//			                                    rtn.setException(exception);
//			                                    isFinished();
//			                                    exception.printStackTrace();
//			                                }
//			
//			                                private void isFinished() {
//			                                	tempFile.delete();
//			                                    LOG.info("All relocations completed");
//			                                    rtn.setFinished();
//			                                }
//			                            });
//			                } catch (IOException e) {
//			                    e.printStackTrace();
//			                    rtn.setException(e);
//			                }
//			            }).start();
//			        	
//			            
//		            } catch (IOException | CryptoException e1) {
//		                e1.printStackTrace();
//		                rtn.setException(e1);
//		            }
//            	}
//	        }
//        }).start();
//        
//        return rtn;
//    }
//}
