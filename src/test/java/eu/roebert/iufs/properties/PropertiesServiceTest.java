package eu.roebert.iufs.properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import eu.roebert.iufs.exceptions.PropertiesException;
import eu.roebert.iufs.properties.PropertiesFileSystemAgent;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.commons.Tuple2;
import jadex.commons.future.ISubscriptionIntermediateFuture;

public class PropertiesServiceTest {
    private static String location = "jUnitTestConfigDir";
    private PropertiesFileSystemAgent classUnderTest;

    @Before
    public void setup() throws PropertiesException {
        classUnderTest = new PropertiesFileSystemAgent();
        classUnderTest.arguments = new IArgumentsResultsFeature() {

            @Override
            public ISubscriptionIntermediateFuture<Tuple2<String, Object>> subscribeToResults() {
                return null;
            }

            @Override
            public Map<String, Object> getResults() {
                return null;
            }

            @Override
            public Map<String, Object> getArguments() {
                HashMap<String, Object> map = new HashMap<>();
                map.put("location", location);
                return map;
            }
        };

        classUnderTest.created();
    }

    @After
    public void afterTest() throws IOException {
        FileUtils.deleteDirectory(new File(location));
    }

    @Test
    public void storeTest() throws PropertiesException {
        classUnderTest.store("foo", "bar");

        assertEquals("bar", classUnderTest.get("foo").get());
        assertTrue(new File(location + "/iufs.properties").exists());
    }

    @Test
    public void getNullTest() {
        Object o = classUnderTest.get("foo").get();

        assertNull(o);
    }

    @Test
    public void mockTest() {
        MockedProperties mock = classUnderTest.getMockedObject();

        mock.setProperty("test", "value");

        assertEquals("value", mock.getProperty("test"));
        assertEquals("value", classUnderTest.get("test").get());
        assertTrue(new File(location + "/iufs.properties").exists());
    }
}
