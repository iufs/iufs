package eu.roebert.iufs.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class FileFlagTest {
    @Test
    public void modelTest() {
        FileFlag flag = new FileFlag("foo", "bar");

        assertEquals(flag, new FileFlag(flag.getFlag(), flag.getValue()));
        assertNotEquals(flag, new FileFlag());
        assertNotEquals(flag, "test");
    }
}
