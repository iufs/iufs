package eu.roebert.iufs.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.Test;

public class FileIDTest {
    @Test
    public void modelTest() {
        FileID id = new FileID("test");
        FileID id2 = new FileID("test");

        assertEquals(id, id2);
        assertNotEquals(id, "test");
    }
}
