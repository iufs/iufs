//package eu.roebert.iufs.io;
//
//import static org.hamcrest.CoreMatchers.*;
//import static org.hamcrest.collection.IsArrayContaining.*;
//import static org.hamcrest.Matchers.hasProperty;
//import static org.junit.Assert.assertArrayEquals;
//import static org.junit.Assert.assertFalse;
//import static org.junit.Assert.assertThat;
//import static org.junit.Assert.assertTrue;
//import static org.mockito.Mockito.when;
//import static org.mockito.Mockito.anyString;
//
//import java.io.File;
//import java.io.IOException;
//import java.nio.charset.StandardCharsets;
//
//import org.apache.commons.io.FileUtils;
//import org.junit.AfterClass;
//import org.junit.Before;
//import org.junit.Test;
//import org.mockito.Mock;
//import org.mockito.MockitoAnnotations;
//
//import eu.roebert.iufs.avscan.AVMiddlewareService;
//import eu.roebert.iufs.exceptions.AVException;
//import eu.roebert.iufs.io.IOFileSystemAgent;
//import eu.roebert.iufs.model.FileFlag;
//import eu.roebert.iufs.model.FileID;
//import eu.roebert.iufs.neighbours.NeighbourhoodService;
//import eu.roebert.iufs.properties.PropertiesFileSystemAgent;
//import eu.roebert.crypto.crypto.Hashing;
//import jadex.commons.future.Future;
//import jadex.commons.future.SubscriptionIntermediateFuture;
//import jadex.micro.annotation.Agent;
//
//@Agent
//public class IOServiceTest {
//    @Mock
//    private PropertiesFileSystemAgent propertiesservice;
//
//    @Mock
//    private AVMiddlewareService avmiddleware;
//
//    @Mock
//    private NeighbourhoodService neighbourhoodservice;
//    
//    
//    private IOFileSystemAgent classUnderTest;
//    
//    private static String rootDir = "jUnitTestRootDir";
//    private int bufferSize = 2048;
//
//    @Before
//    public void setup() {
//        MockitoAnnotations.initMocks(this);
//        classUnderTest = new IOFileSystemAgent();
//        classUnderTest.propertiesservice = propertiesservice;
//        classUnderTest.avmiddleware = avmiddleware;
//        
//        when(propertiesservice.get("rootDir")).thenReturn(new Future<>(rootDir));
//        when(propertiesservice.getInt("bufferSize")).thenReturn(new Future<>(bufferSize));
//        try {
//			when(avmiddleware.scan(anyString())).thenReturn(true);
//		} catch (AVException e) {
//			e.printStackTrace();
//		}
//
//
//        classUnderTest.created();
//    }
//
//    @AfterClass
//    public static void afterTest() throws IOException {
//        FileUtils.deleteDirectory(new File(rootDir));
//    }
//
//    private FileID saveFile() throws IOException {
//        String data = "test";
//        byte[] dataByte = data.getBytes(StandardCharsets.UTF_8);
//        FileID id = new FileID(Hashing.sha256(dataByte));
//
//        FileUtils.writeByteArrayToFile(new File(rootDir + "/" + id.getId() + "/" + id.getId() + ".iufs"), dataByte);
//
//        return id;
//    }
//
//    @Test
//    public void saveTest() throws IOException {
//        String data = "test";
//        String ext = "txt";
//        byte[] dataByte = data.getBytes(StandardCharsets.UTF_8);
//        FileID id = new FileID(Hashing.sha256(dataByte));
//
//        File tmpDir = new File(rootDir + "/test");
//        tmpDir.mkdirs();
//        File filePath = new File(rootDir + "/test/" + id.getId() + "." + ext);
//        FileUtils.writeByteArrayToFile(filePath, dataByte);
//
//        Future<FileID> fid = new Future<>();
//
//        SubscriptionIntermediateFuture<byte[]> fileStream = new SubscriptionIntermediateFuture<>();
//
//        classUnderTest.getHelperStream(filePath.getAbsolutePath(), fileStream);
//        classUnderTest.saveHelper(fileStream, "txt", fid);
//
//        assertTrue(new File(rootDir + "/" + id.getId() + "/" + id.getId() + ".iufs").exists());
//        assertTrue(classUnderTest.fileExists(id).get());
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("flag", is("extension"))));
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("value", is("txt"))));
//
//        classUnderTest.delete(id);
//    }
//
//    @Test
//    public void deleteTest() throws IOException {
//        FileID id = saveFile();
//        classUnderTest.delete(id);
//
//        assertFalse(new File(rootDir + "/" + id.getId() + "/" + id.getId() + ".iufs").exists());
//        assertFalse(classUnderTest.fileExists(id).get());
//    }
//
//    @Test
//    public void getTest() throws IOException {
//        FileID id = saveFile();
//        SubscriptionIntermediateFuture<byte[]> fileStream = new SubscriptionIntermediateFuture<>();
//        classUnderTest.getHelperStream(
//                new File(rootDir + "/" + id.getId() + "/" + id.getId() + ".iufs").getAbsolutePath(), fileStream);
//
//        // Dieser Test geht nur für Daten die kleiner als bufferSize sind
//        fileStream.addIntermediateResultListener(result -> {
//            assertArrayEquals("test".getBytes(StandardCharsets.UTF_8), result);
//        });
//
//        classUnderTest.delete(id);
//    }
//
//    @Test
//    public void flagTest() throws IOException {
//        FileFlag[] flags = new FileFlag[] { new FileFlag("immutable", ""), new FileFlag("owner", "me") };
//        FileID id = saveFile();
//
//        classUnderTest.setFlags(id, flags);
//
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("flag", is("owner"))));
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("value", is("me"))));
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("flag", is("immutable"))));
//        classUnderTest.delete(id);
//    }
//
//    @Test
//    public void oneFlagTest() throws IOException {
//        FileFlag flag = new FileFlag("immutable", "");
//        FileID id = saveFile();
//
//        classUnderTest.setFlag(id, flag);
//
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("flag", is("immutable"))));
//        classUnderTest.delete(id);
//    }
//
//    @Test
//    public void doubleSpendFlagTest() throws IOException {
//        FileFlag flag = new FileFlag("immutable", "");
//        FileID id = saveFile();
//
//        classUnderTest.setFlag(id, flag);
//        classUnderTest.setFlag(id, new FileFlag("immutable", "false"));
//
//        assertThat(classUnderTest.getFlags(id).get(), hasItemInArray(hasProperty("value", is("false"))));
//
//        classUnderTest.delete(id);
//    }
//}
