package eu.roebert.iufs.filedb;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import eu.roebert.crypto.crypto.Hashing;
import eu.roebert.iufs.model.FileFlag;
import eu.roebert.iufs.model.FileID;
import jadex.bridge.component.IArgumentsResultsFeature;
import jadex.commons.Tuple2;
import jadex.commons.future.ISubscriptionIntermediateFuture;

public class FileDBAgentTest {
    private static String fileDBTestPath;
    private static FileDBAgent fileDBAgent;

    private static FileID testFileID1;
    private static FileID testFileID2;
    private static FileID testFileID3;
    private static FileID testFileID4;
    private static FileID testFileID5;

    private static String testFlagKey1 = "testFlagKey1";
    private static String testFlagKey2 = "testFlagKey2";
    private static String testFlagValue1 = "testFlagValue1";
    private static String testFlagValue2 = "testFlagValue2";
    private static String testFlagValue3 = "testFlagValue3";

    private static List<FileID> testFileIDs;

    @ClassRule
    public static TemporaryFolder tempFolder = new TemporaryFolder(new File(System.getProperty("user.dir")));

    @BeforeClass
    public static void setUpDatabase() throws IOException {
        // Create folder
        tempFolder.newFolder("testFileDB");
        fileDBTestPath = tempFolder.getRoot().getPath();

        fileDBAgent = new FileDBAgent();
        fileDBAgent.arguments = new IArgumentsResultsFeature() {
            @Override
            public ISubscriptionIntermediateFuture<Tuple2<String, Object>> subscribeToResults() {
                return null;
            }

            @Override
            public Map<String, Object> getResults() {
                return null;
            }

            @Override
            public Map<String, Object> getArguments() {
                HashMap<String, Object> map = new HashMap<>();
                map.put("fileDBPath", fileDBTestPath);
                return map;
            }
        };
        fileDBAgent.created();

        testFileID1 = generateFileID();
        testFileID2 = generateFileID();
        testFileID3 = generateFileID();
        testFileID4 = generateFileID();
        testFileID5 = generateFileID();
        
        testFileIDs = Arrays.asList(testFileID1, testFileID2, testFileID3, testFileID4);
        
        fileDBAgent.insert(testFileID1);
        fileDBAgent.insert(testFileID2);
        fileDBAgent.insert(testFileID3);
        fileDBAgent.insert(testFileID4);
    }

    @AfterClass
    public static void afterTest() throws IOException {
        FileDBAgent.fileDB.close();
    }
    
    private static FileID generateFileID() {
        byte[] dataBytes = new byte[4];
        new Random().nextBytes(dataBytes);
        return new FileID(Hashing.sha256(dataBytes));
    }

    @Test
    public void createDatabaseTest() {
        assertTrue(new File(fileDBTestPath + "/CURRENT").exists());
    }

    @Test
    public void multipleInsertsPossibleTest() throws IOException {
        fileDBAgent.insert(testFileID1);
        fileDBAgent.insert(testFileID2);
        
        List<FileID> result = fileDBAgent.getAllKeys().get();

        assertTrue(result.containsAll(testFileIDs));
        assertTrue(result.size() == 4);
    }

    @Test
    public void getAllKeysTest() throws IOException {
        List<FileID> result = fileDBAgent.getAllKeys().get();

        assertTrue(result.containsAll(testFileIDs));
        assertTrue(result.size() == 4);
    }

    @Test
    public void deleteTest() throws IOException {
        fileDBAgent.insert(testFileID5);
        List<FileID> result = fileDBAgent.getAllKeys().get();

        assertTrue(result.containsAll(testFileIDs));
        assertTrue(result.contains(testFileID5));
        assertTrue(result.size() == 5);

        fileDBAgent.delete(testFileID5);
        result = fileDBAgent.getAllKeys().get();

        assertTrue(result.containsAll(testFileIDs));
        assertTrue(result.size() == 4);
    }

    @Test
    public void setGetFlagTest() throws IOException {
        fileDBAgent.setFlag(testFileID3, new FileFlag(testFlagKey1, testFlagValue1));
        fileDBAgent.setFlag(testFileID3, new FileFlag(testFlagKey2, testFlagValue2));

        assertEquals(fileDBAgent.getFlag(testFileID3, testFlagKey1, "").get(), testFlagValue1);
        assertEquals(fileDBAgent.getFlag(testFileID3, testFlagKey2, "").get(), testFlagValue2);
        
        fileDBAgent.setFlag(testFileID3, new FileFlag(testFlagKey1, testFlagValue3));
        
        assertEquals(fileDBAgent.getFlag(testFileID3, testFlagKey1, "").get(), testFlagValue3);
        assertEquals(fileDBAgent.getFlag(testFileID3, testFlagKey2, "").get(), testFlagValue2);
        
        assertEquals("defaultTestValue1", fileDBAgent.getFlag(testFileID4, testFlagKey1, "defaultTestValue1").get());
        assertEquals("defaultTestValue2", fileDBAgent.getFlag(testFileID4, testFlagKey2, "defaultTestValue2").get());
    }
    
    @Test
    public void setGetFlagsTest() throws IOException {
        List<FileFlag> flags = Arrays.asList(new FileFlag(testFlagKey1, testFlagValue1), new FileFlag(testFlagKey2, testFlagValue2));
        fileDBAgent.setFlags(testFileID1, flags);
        
        assertEquals(fileDBAgent.getFlag(testFileID1, testFlagKey1, "").get(), testFlagValue1);
        assertEquals(fileDBAgent.getFlag(testFileID1, testFlagKey2, "").get(), testFlagValue2);

        flags = Arrays.asList(new FileFlag(testFlagKey2, testFlagValue3), new FileFlag(testFlagKey1, testFlagValue1));
        fileDBAgent.setFlags(testFileID1, flags);
        
        assertEquals(fileDBAgent.getFlag(testFileID1, testFlagKey1, "").get(), testFlagValue1);
        assertEquals(fileDBAgent.getFlag(testFileID1, testFlagKey2, "").get(), testFlagValue3);

        assertEquals("defaultTestValue1", fileDBAgent.getFlag(testFileID2, testFlagKey1, "defaultTestValue1").get());
        assertEquals("defaultTestValue2", fileDBAgent.getFlag(testFileID2, testFlagKey2, "defaultTestValue2").get());
    }
}