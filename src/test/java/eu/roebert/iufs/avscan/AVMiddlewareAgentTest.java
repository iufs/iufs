package eu.roebert.iufs.avscan;

import java.util.ArrayList;

import static org.mockito.Mockito.when;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.anyString;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.roebert.iufs.exceptions.AVException;
import jadex.commons.future.Future;

public class AVMiddlewareAgentTest {
    @Mock
    private AVScanService avService;

    @Mock
    private IAVDatabaseService avdatabase;

    private AVMiddlewareAgent classUnderTest;

    @Before
    public void setup() throws AVException {
        MockitoAnnotations.initMocks(this);
        classUnderTest = new AVMiddlewareAgent();
        classUnderTest.avs = new ArrayList<AVScanService>();
        classUnderTest.avs.add(avService);
        classUnderTest.avdatabase = avdatabase;

        when(avService.available()).thenReturn(true);
        when(avService.scan(anyString(), anyString())).thenReturn(true);
        when(avdatabase.increment(anyString())).thenReturn(Future.DONE);
    }

    @Test
    public void scanTest() throws AVException {
        assertTrue(classUnderTest.scan("id", "foo"));
    }

    @Test
    public void scanTest2() throws AVException {
        when(avService.scan(anyString(), anyString())).thenReturn(false);
        assertFalse(classUnderTest.scan("id", "foo"));
    }
}
