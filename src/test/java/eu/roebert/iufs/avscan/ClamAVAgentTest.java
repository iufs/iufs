package eu.roebert.iufs.avscan;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import eu.roebert.iufs.exceptions.AVException;
import eu.roebert.iufs.properties.IPropertiesService;
import jadex.commons.future.Future;

public class ClamAVAgentTest {
    @Mock
    private IPropertiesService propertiesservice;

    private ClamAVAgent classUnderTest;
    private static String rootDir = "localhost";
    private int bufferSize = 3310;
    private boolean enabled = true;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        classUnderTest = new ClamAVAgent();
        classUnderTest.propertiesservice = propertiesservice;

        when(propertiesservice.get("clamAVHost")).thenReturn(new Future<>(rootDir));
        when(propertiesservice.getInt("clamAVPort")).thenReturn(new Future<>(bufferSize));
        when(propertiesservice.getBool("clamAVEnabled")).thenReturn(new Future<>(enabled));

        classUnderTest.created();
    }

    @Test
    public void virusDetectionTest() throws AVException {
        if (classUnderTest.available()) {
            String virus = "X5O!P%@AP[4\\PZX54(P^)7CC)7}$EICAR-STANDARD-ANTIVIRUS-TEST-FILE!$H+H*";
            String noVirus = "foo";

            assertFalse(classUnderTest.scan(new ByteArrayInputStream(virus.getBytes())));
            assertTrue(classUnderTest.scan(new ByteArrayInputStream(noVirus.getBytes())));
        }
    }
}
