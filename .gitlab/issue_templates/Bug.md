<!--
Thanks for wanting to report an issue you've found. Please delete
this text and fill in the template below. If unsure about something, just do as
best as you're able.

Note that it will be much easier for us to fix the issue if a test case that
reproduces the problem is provided. Ideally this test case should not have any
external dependencies. We understand that it is not always possible to reduce
your code to a small test case, but we would appreciate to have as much data as
possible. Thank you!
-->

### Prerequisites
* [ ] Can you reproduce the problem in a **minimal** example with **only** IUFS as dependency?
* [ ] Are you running the latest stable version of IUFS?
* [ ] Are you have changed settings under `iufs.properties`?

### Description

[Description of the bug or feature]

### Steps to Reproduce

1. [First Step]
2. [Second Step]
3. [and so on...]

**Expected behavior:** [What you expected to happen]

**Actual behavior:** [What actually happened]

### System
* **Version**: [compulsory. you must provide your version]
* **Platform**: [either `uname -a` output, or if Windows, version and 32-bit or
  64-bit]
* **pom.xml**: [Please provide your maven pom.xml file]

### Log
<!-- Please enable the log functionality of IUFS and attach the log at this issue. -->