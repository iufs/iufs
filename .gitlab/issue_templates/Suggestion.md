<!--
Thanks for wanting to submit a suggestion. Please delete
this text and fill in the template below. If unsure about something, just do as
best as you're able.
-->

### Description

[Description of the suggestion]

### Affected files

[For example, the AVScanService or the FileContainer.]

### Possible incompatibilities

[For example, after change file size is limited to 1GB.]